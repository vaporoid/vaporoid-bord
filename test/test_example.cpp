#include <iostream>
#include <boost/current_function.hpp>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(test) {
  std::cout << BOOST_CURRENT_FUNCTION << "\n";
}
