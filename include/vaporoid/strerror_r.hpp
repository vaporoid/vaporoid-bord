#ifndef VAPOROID_STRERROR_R_HPP
#define VAPOROID_STRERROR_R_HPP

#include <errno.h>
#include <stddef.h>
#include <string.h>

namespace vaporoid {
  inline const char* strerror_r_impl(const char*, const char* result) {
    return result;
  }

  inline const char* strerror_r_impl(const char* data, int result) {
    if (result == 0) {
      return data;
    }
    if (result != -1) {
      errno = result;
    }
    return 0;
  }

  inline const char* strerror_r(int code, char* data, size_t size) {
    return strerror_r_impl(data, ::strerror_r(code, data, size));
  }
}

#endif
