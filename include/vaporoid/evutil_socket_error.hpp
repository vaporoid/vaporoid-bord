#ifndef VAPOROID_EVUTIL_SOCKET_ERROR_HPP
#define VAPOROID_EVUTIL_SOCKET_ERROR_HPP

#include <event2/util.h>
#include <string>

namespace vaporoid {
  class evutil_socket_error : public std::runtime_error {
  public:
    explicit evutil_socket_error(const std::string& what, int code = EVUTIL_SOCKET_ERROR())
      : std::runtime_error(what), code_(code) {}

    virtual ~evutil_socket_error() throw() {}

    int code() const {
      return code_;
    }

    virtual const char* what() const throw() {
      try {
        if (what_.empty()) {
          std::ostringstream out;
          out << std::runtime_error::what() << ": ";

          if (const char* message = evutil_socket_error_to_string(code_)) {
            out << message;
          } else {
            out << "error number " << code_;
          }

          what_ = out.str();
        }
        return what_.c_str();
      } catch (...) {
        return std::runtime_error::what();
      }
    }

  private:
    int code_;
    mutable std::string what_;
  };
}

#endif
