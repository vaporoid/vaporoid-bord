#ifndef VAPOROID_SYSTEM_ERROR_HPP
#define VAPOROID_SYSTEM_ERROR_HPP

#include <errno.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <boost/array.hpp>
#include <vaporoid/strerror_r.hpp>

namespace vaporoid {
  class system_error : public std::runtime_error {
  public:
    explicit system_error(const std::string& what, int code = errno)
      : std::runtime_error(what), code_(code) {}

    virtual ~system_error() throw() {}

    int code() const {
      return code_;
    }

    virtual const char* what() const throw() {
      try {
        if (what_.empty()) {
          std::ostringstream out;
          out << std::runtime_error::what() << ": ";

          boost::array<char, 64> static_buffer;
          if (!print_error_message_(out, code_, static_buffer)) {
            std::vector<char> dynamic_buffer(static_buffer.size() * 2);
            while (!print_error_message_(out, code_, dynamic_buffer)) {
              dynamic_buffer.resize(dynamic_buffer.size() * 2);
            }
          }

          what_ = out.str();
        }
        return what_.c_str();
      } catch (...) {
        return std::runtime_error::what();
      }
    }

  private:
    int code_;
    mutable std::string what_;

    template <typename U>
    static bool print_error_message_(std::ostream& out, int code, U& buffer) {
      errno = 0;
      if (const char* message = vaporoid::strerror_r(code, &buffer[0], buffer.size())) {
        out << message;
      } else if (errno != ERANGE) {
        out << "error number " << code;
      } else {
        return false;
      }
      return true;
    }
  };
}

#endif
