#ifndef VAPOROID_QUERY_HPP
#define VAPOROID_QUERY_HPP

#include <cstdlib>
#include <map>
#include <string>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/xpressive/xpressive.hpp>

namespace vaporoid {
  namespace query_detail {
    inline std::string decode(const std::string& source) {
      using boost::xpressive::as_xpr;
      using boost::xpressive::regex_replace;;
      using boost::xpressive::smatch;
      using boost::xpressive::sregex;
      using boost::xpressive::xdigit;

      static const sregex re = as_xpr('%') >> xdigit >> xdigit | as_xpr('+');
      return regex_replace(
          source,
          re,
          [](const boost::xpressive::smatch& what) {
            std::string s = what.str();
            if (s == "+") {
              return std::string(" ");
            } else {
              char c = std::strtol(s.c_str() + 1, 0, 16);
              return std::string(&c, 1);
            }
          });
    }
  }

  inline std::map<std::string, std::string> parse_query(const std::string& source) {
    typedef boost::char_separator<char> separator;
    typedef boost::tokenizer<separator> tokenizer;
    using query_detail::decode;

    std::map<std::string, std::string> target;
    BOOST_FOREACH(const std::string& i, tokenizer(source, separator("&"))) {
      size_t j = i.find("=");
      if (j == std::string::npos) {
        target[decode(i)] = std::string();
      } else {
        target[decode(i.substr(0, j))] = decode(i.substr(j + 1));
      }
    }
    return target;
  }
}

#endif
