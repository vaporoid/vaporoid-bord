#ifndef VAPOROID_ADDRINFO_HPP
#define VAPOROID_ADDRINFO_HPP

#include <vaporoid/addrinfo/addrinfo_error.hpp>
#include <vaporoid/addrinfo/addrinfo_iterator.hpp>
#include <vaporoid/addrinfo/addrinfo_query.hpp>
#include <vaporoid/addrinfo/make_addrinfo_iterator_range.hpp>

#endif
