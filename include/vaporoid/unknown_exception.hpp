#ifndef VAPOROID_UNKNOWN_EXCEPTION_HPP
#define VAPOROID_UNKNOWN_EXCEPTION_HPP

#include <exception>

namespace vaporoid {
  class unknown_exception : public std::exception {
  public:
    virtual ~unknown_exception() throw() {}

    virtual const char* what() const throw() {
      return "unknown exception";
    }
  };
}

#endif
