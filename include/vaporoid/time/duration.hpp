#ifndef VAPOROID_TIME_DURATION_HPP
#define VAPOROID_TIME_DURATION_HPP

#include <stdint.h>
#include <ostream>
#include <boost/operators.hpp>
#include <vaporoid/time/time_impl.hpp>

namespace vaporoid {
  namespace time {
    class duration
      : public time_impl,
        boost::totally_ordered<duration,
        boost::additive<duration,
        boost::multiplicative2<duration, uint32_t> > > {
      friend class point;
    public:
      typedef time_impl base_type;
      typedef duration  this_type;

      explicit duration(int64_t time = 0, uint32_t nano = 0, uint32_t atto = 0)
        : base_type(time, nano, atto) {}

      bool operator<(const this_type& rhs) const {
        return less(rhs);
      }

      bool operator==(const this_type& rhs) const {
        return equal(rhs);
      }

      this_type operator-() const {
        this_type tmp(*this);
        tmp.minus();
        return tmp;
      }

      this_type& operator+=(const this_type& rhs) {
        add(rhs);
        return *this;
      }

      this_type& operator-=(const this_type& rhs) {
        sub(rhs);
        return *this;
      }

      this_type& operator*=(uint32_t rhs) {
        mul(rhs);
        return *this;
      }

      this_type& operator/=(uint32_t rhs) {
        div(rhs);
        return *this;
      }

      friend std::ostream& operator<<(std::ostream& out, const this_type& self) {
        self.print_(out);
        return out;
      }

    private:
      explicit duration(const time_impl& lhs, const time_impl& rhs)
        : base_type(lhs) {
        sub(rhs);
      }

      void print_(std::ostream& out) const {
        if (time() < 0) {
          this_type tmp(*this);
          tmp.minus();
          out << "-" << tmp.time();
          tmp.print_nano_atto(out);
        } else {
          out << time();
          print_nano_atto(out);
        }
      }
    };
  }
}

#endif
