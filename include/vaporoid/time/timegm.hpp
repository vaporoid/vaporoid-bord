#ifndef VAPOROID_TIME_TIMEGM_IMPL_HPP
#define VAPOROID_TIME_TIMEGM_IMPL_HPP

#include <time.h>
#include <vaporoid/config.hpp>
#include <vaporoid/time/jdn.hpp>

namespace vaporoid {
  namespace time {
    inline time_t timegm_impl(struct tm* tm) {
      time_t t = calendar_to_jdn(tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday) - 2440588;
      return t * 86400 + tm->tm_hour * 3600 + tm->tm_min * 60 + tm->tm_sec;
    }

    inline time_t timegm(struct tm* tm) {
#if VAPOROID_CONFIG_HAS_TIMEGM - 0 != 0
      return ::timegm(tm);
#else
      return timegm_impl(tm);
#endif
    }
  }
}

#endif
