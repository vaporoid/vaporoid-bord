#ifndef VAPOROID_TIME_NOW_HPP
#define VAPOROID_TIME_NOW_HPP

namespace vaporoid {
  namespace time {
    struct now_t {};

    namespace {
      const now_t now;
    }
  }
}

#endif
