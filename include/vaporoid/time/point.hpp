#ifndef VAPOROID_TIME_POINT_HPP
#define VAPOROID_TIME_POINT_HPP

#include <stdint.h>
#include <sys/time.h>
#include <time.h>
#include <iomanip>
#include <ostream>
#include <boost/io/ios_state.hpp>
#include <boost/operators.hpp>
#include <boost/utility/value_init.hpp>
#include <vaporoid/system_error.hpp>
#include <vaporoid/time/duration.hpp>
#include <vaporoid/time/now.hpp>
#include <vaporoid/time/time_impl.hpp>
#include <vaporoid/time/timegm.hpp>

namespace vaporoid {
  namespace time {
    class point
      : public time_impl,
        boost::totally_ordered<point,
        boost::additive2<point, duration> > {
    public:
      typedef time_impl  base_type;
      typedef point this_type;

      explicit point(int64_t time = 0, uint32_t nano = 0, uint32_t atto = 0)
        : base_type(time, nano, atto) {}

      explicit point(const now_t&)
        : base_type(0, 0, 0) {
        struct timeval tv = boost::initialized_value;
        if (gettimeofday(&tv, 0) == -1) {
          throw system_error("could not gettimeofday");
        }
        set_time(tv.tv_sec);
        set_nano(tv.tv_usec * 1000);
      }

      explicit point(const struct tm& tm, uint32_t nano = 0, uint32_t atto = 0)
        : base_type(0, nano, atto) {
        struct tm tmp = tm;
        time_t t = vaporoid::time::timegm(&tmp);
        if (t == static_cast<time_t>(-1)) {
          throw system_error("could not timegm");
        }
        set_time(t);
      }

      void get(struct tm& tm) const {
        tm = boost::initialized_value;
        time_t t = time();
        if (!gmtime_r(&t, &tm)) {
          throw system_error("could not gmtime_r");
        }
      }

      bool operator<(const this_type& rhs) const {
        return less(rhs);
      }

      bool operator==(const this_type& rhs) const {
        return equal(rhs);
      }

      this_type& operator+=(const duration& rhs) {
        add(rhs);
        return *this;
      }

      this_type& operator-=(const duration& rhs) {
        sub(rhs);
        return *this;
      }

      friend duration operator-(const this_type& lhs, const this_type& rhs) {
        return lhs.sub_(rhs);
      }

      friend std::ostream& operator<<(std::ostream& out, const this_type& self) {
        self.print_(out);
        return out;
      }

    private:
      duration sub_(const this_type& rhs) const {
        return duration(*this, rhs);
      }

      void print_(std::ostream& out) const {
        struct tm tm;
        get(tm);

        {
          boost::io::ios_fill_saver save(out);
          out << std::setfill('0')
              << tm.tm_year + 1900
              << "-"
              << std::setw(2) << tm.tm_mon + 1
              << "-"
              << std::setw(2) << tm.tm_mday
              << "T"
              << std::setw(2) << tm.tm_hour
              << ":"
              << std::setw(2) << tm.tm_min
              << ":"
              << std::setw(2) << tm.tm_sec;
        }
        print_nano_atto(out);
        out << "Z";
      }
    };
  }
}

#endif
