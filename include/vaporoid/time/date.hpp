#ifndef VAPOROID_TIME_DATE_HPP
#define VAPOROID_TIME_DATE_HPP

#include <time.h>
#include <iomanip>
#include <ostream>
#include <boost/io/ios_state.hpp>
#include <boost/operators.hpp>
#include <boost/utility/value_init.hpp>
#include <vaporoid/time/jdn.hpp>

namespace vaporoid {
  namespace time {
    class date
      : boost::totally_ordered<date,
        boost::additive2<date, int> > {
    public:
      typedef date this_type;

      explicit date(int year, int month, int day)
        : jdn_(calendar_to_jdn(year, month, day)) {}

      explicit date(const struct tm& tm)
        : jdn_(calendar_to_jdn(tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday)) {}

      void get(struct tm& tm) const {
        int year, month, day;
        jdn_to_calendar(jdn_, year, month, day);

        tm = boost::initialized_value;
        tm.tm_year = year - 1900;
        tm.tm_mon  = month - 1;
        tm.tm_mday = day;
      }

      bool operator<(const this_type& rhs) const {
        return jdn_ < rhs.jdn_;
      }

      bool operator==(const this_type& rhs) const {
        return jdn_ == rhs.jdn_;
      }

      this_type& operator+=(int rhs) {
        jdn_ += rhs;
        return *this;
      }

      this_type& operator-=(int rhs) {
        jdn_ -= rhs;
        return *this;
      }

      friend int operator-(const this_type& lhs, const this_type& rhs) {
        return lhs.jdn_ - rhs.jdn_;
      }

      friend std::ostream& operator<<(std::ostream& out, const this_type& self) {
        self.print_(out);
        return out;
      }

    private:
      int jdn_;

      void print_(std::ostream& out) const {
        int year, month, day;
        jdn_to_calendar(jdn_, year, month, day);

        boost::io::ios_fill_saver save(out);
        out << std::setfill('0')
            << year
            << "-"
            << std::setw(2) << month
            << "-"
            << std::setw(2) << day;
      }
    };
  }
}

#endif
