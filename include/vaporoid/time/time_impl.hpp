#ifndef VAPOROID_TIME_TIME_IMPL_HPP
#define VAPOROID_TIME_TIME_IMPL_HPP

#include <stdint.h>
#include <sys/time.h>
#include <time.h>
#include <iomanip>
#include <ostream>
#include <boost/io/ios_state.hpp>
#include <boost/utility/value_init.hpp>

namespace vaporoid {
  namespace time {
    class time_impl {
    public:
      int64_t time() const {
        return time_;
      }

      uint32_t nano() const {
        return nano_;
      }

      uint32_t atto() const {
        return atto_;
      }

      void get(struct timespec& tv) const {
        tv = boost::initialized_value;
        tv.tv_sec = time_;
        tv.tv_nsec = nano_;
      }

      void get(struct timeval& tv) const {
        tv = boost::initialized_value;
        tv.tv_sec = time_;
        tv.tv_usec = nano_ / 1000;
      }

    protected:
      explicit time_impl(int64_t time, uint32_t nano, uint32_t atto)
        : time_(time), nano_(nano), atto_(atto) {}

      void set_time(int64_t time) {
        time_ = time;
      }

      void set_nano(uint32_t nano) {
        nano_ = nano;
      }

      void set_atto(uint32_t atto) {
        atto_ = atto;
      }

      bool less(const time_impl& rhs) const {
        if (time_ == rhs.time_) {
          if (nano_ == rhs.nano_) {
            return atto_ < rhs.atto_;
          }
          return nano_ < rhs.nano_;
        }
        return time_ < rhs.time_;
      }

      bool equal(const time_impl& rhs) const {
        return time_ == rhs.time_ && nano_ == rhs.nano_ && atto_ == rhs.atto_;
      }

      void minus() {
        if (atto_ > 0) {
          atto_ = 1000000000 - atto_;
          ++nano_;
        }
        if (nano_ > 0) {
          nano_ = 1000000000 - nano_;
          ++time_;
        }
        time_ = -time_;
      }

      void add(const time_impl& rhs) {
        time_ += rhs.time_;
        nano_ += rhs.nano_;
        atto_ += rhs.atto_;

        if (atto_ > 999999999) {
          atto_ -= 1000000000;
          ++nano_;
        }
        if (nano_ > 999999999) {
          nano_ -= 1000000000;
          ++time_;
        }
      }

      void sub(const time_impl& rhs) {
        time_ -= rhs.time_;
        nano_ -= rhs.nano_;
        atto_ -= rhs.atto_;

        if (atto_ < 0) {
          atto_ += 1000000000;
          --nano_;
        }
        if (nano_ < 0) {
          nano_ += 1000000000;
          --time_;
        }
      }

      void mul(uint32_t rhs) {
        int64_t v = rhs;
        int64_t atto = atto_ * v;
        int64_t nano = nano_ * v + atto / 1000000000;
        int64_t time = time_ * v + nano / 1000000000;

        time_ = time;
        nano_ = nano % 1000000000;
        atto_ = atto % 1000000000;
      }

      void div(uint32_t rhs) {
        int64_t v = time_ < 0 ? rhs - 1 : 0;
        int64_t time = (time_ - v) / rhs;
        int64_t nano = nano_ + (time_ - time * rhs) * 1000000000;
        int64_t atto = atto_ + (nano % rhs) * 1000000000 + v;

        time_ = time;
        nano_ = nano / rhs;
        atto_ = atto / rhs;
      }

      void print_nano_atto(std::ostream& out) const {
        if (nano_ == 0 && atto_ == 0) {
          return;
        }

        boost::io::ios_fill_saver save(out);
        out << std::setfill('0') << ".";
        if (atto_ > 0) {
          if (nano_ > 0) {
            out << std::setw(9) << nano_;
          } else {
            out << "000000000";
          }
          print_(out, atto_);
        } else {
          print_(out, nano_);
        }
      }

    private:
      int64_t time_;
      int32_t nano_;
      int32_t atto_;

      static void print_(std::ostream& out, int32_t value) {
        if (value % 1000000 == 0) {
          out << std::setw(3) << value / 1000000;
        } else if (value % 1000 == 0) {
          out << std::setw(6) << value / 1000;
        } else {
          out << std::setw(9) << value;
        }
      }
    };
  }
}

#endif
