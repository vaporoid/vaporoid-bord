#ifndef VAPOROID_TIME_JDN_HPP
#define VAPOROID_TIME_JDN_HPP

namespace vaporoid {
  namespace time {
    inline bool is_julian_calendar(int year, int month, int day) {
      if (year == 1582) {
        if (month == 10) {
          return day < 15;
        }
        return month < 10;
      }
      return year < 1582;
    }

    inline int calendar_to_jdn(int year, int month, int day) {
      int a = (14 - month) / 12;
      int y = year + 4800 - a;
      int m = month + 12 * a - 3;

      if (is_julian_calendar(year, month, day)) {
        return day + (153 * m + 2) / 5 + 365 * y + y / 4 - 32083;
      } else {
        return day + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045;
      }
    }

    inline void jdn_to_calendar(int J, int& year, int& month, int& day) {
      static const int y = 4716;
      static const int j = 1401;
      static const int m = 2;
      static const int n = 12;
      static const int r = 4;
      static const int p = 1461;
      static const int v = 3;
      static const int u = 5;
      static const int s = 153;
      static const int w = 2;
      static const int B = 274277;
      static const int C = -38;

      const int f = J + j + (4 * J + B) / 146097 * 3 / 4 + C;
      const int e = r * f + v;
      const int g = e % p / r;
      const int h = u * g + w;
      const int D = h % s / u + 1;
      const int M = (h / s + m) % n + 1;
      const int Y = e / p - y + (n + m - M) / n;

      year  = Y;
      month = M;
      day   = D;
    }
  }
}

#endif
