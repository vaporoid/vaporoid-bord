#ifndef VAPOROID_UNEXPECTED_HPP
#define VAPOROID_UNEXPECTED_HPP

#include <exception>
#include <iostream>
#include <boost/current_function.hpp>
#include <vaporoid/unknown_exception.hpp>

namespace vaporoid {
  typedef void (*unexpected_handler_type)(const std::exception& e, const char* file, int line, const char* function);

  inline void unexpected_handler_cerr(const std::exception& e, const char* file, int line, const char* function) {
    std::cerr
        << "[vaporoid::unexpected] "
        << e.what()
        << " in function "
        << function
        << " at file "
        << file
        << " line "
        << line
        << std::endl;
  }

  inline unexpected_handler_type unexpected_impl(bool set, unexpected_handler_type handler) {
    static unexpected_handler_type handler_;
    if (set) {
      handler_ = handler;
    }
    return handler_;
  }

  inline unexpected_handler_type get_unexpected() {
    return unexpected_impl(false, 0);
  }

  inline void set_unexpected(unexpected_handler_type handler) {
    unexpected_impl(true, handler);
  }

  inline void unexpected(const std::exception& e, const char* file, int line, const char* function) {
    try {
      if (unexpected_handler_type handler = get_unexpected()) {
        handler(e, file, line, function);
      }
    } catch (...) {}
  }
}

#define VAPOROID_UNEXPECTED(e) \
  vaporoid::unexpected(e, __FILE__, __LINE__, BOOST_CURRENT_FUNCTION) \
/**/

#define VAPOROID_UNEXPECTED_BEGIN \
  try { \
/**/

#define VAPOROID_UNEXPECTED_END \
  } catch (const std::exception& e) {\
    VAPOROID_UNEXPECTED(e); \
  } catch (...) { \
    VAPOROID_UNEXPECTED(vaporoid::unknown_exception()); \
  } \
/**/

#endif
