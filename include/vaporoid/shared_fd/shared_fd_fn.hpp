#ifndef VAPOROID_SHARED_FD_SHARED_FD_FN_HPP
#define VAPOROID_SHARED_FD_SHARED_FD_FN_HPP

#include <vaporoid/nothrow_close.hpp>
#include <vaporoid/shared_fd/shared_fd_count.hpp>

namespace vaporoid {
  namespace shared_fd_detail {
    template <typename T, typename T_fn = void>
    class shared_fd_fn : public shared_fd_count<T> {
    public:
      explicit shared_fd_fn(int fd, const T_fn& fn)
        : shared_fd_count<T>(fd), fn_(fn) {}

      virtual ~shared_fd_fn() {
        fn_(this->get());
      }

    private:
      T_fn fn_;
    };

    template <typename T>
    class shared_fd_fn<T, void> : public shared_fd_count<T> {
    public:
      explicit shared_fd_fn(int fd)
        : shared_fd_count<T>(fd) {}

      virtual ~shared_fd_fn() {
        nothrow_close(this->get());
      }
    };
  }
}

#endif
