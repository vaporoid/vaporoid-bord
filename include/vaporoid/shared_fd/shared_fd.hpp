#ifndef VAPOROID_SHARED_FD_SHARED_FD_HPP
#define VAPOROID_SHARED_FD_SHARED_FD_HPP

#include <stddef.h>
#include <boost/atomic.hpp>
#include <boost/checked_delete.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/swap.hpp>
#include <boost/type_traits/decay.hpp>
#include <vaporoid/shared_fd/shared_fd_count.hpp>
#include <vaporoid/shared_fd/shared_fd_fn.hpp>

namespace vaporoid {
  namespace shared_fd_detail {
    template <typename T>
    class shared_fd {
    public:
      typedef int (shared_fd::*unspecified_bool_type)() const;

      explicit shared_fd() {}

      explicit shared_fd(int fd) {
        typedef shared_fd_fn<T> U;

        if (fd == -1) {
          return;
        }

        shared_fd_count<T>* ptr = 0;
        try {
          ptr = new U(fd);
          ptr_.reset(ptr);
        } catch (...) {
          boost::checked_delete(ptr);
          nothrow_close(fd);
          throw;
        }
      }

      template <typename U_fn>
      explicit shared_fd(int fd, const U_fn& fn) {
        typedef shared_fd_fn<T, typename boost::decay<U_fn>::type> U;

        if (fd == -1) {
          return;
        }

        shared_fd_count<T>* ptr = 0;
        try {
          ptr = new U(fd, fn);
          ptr_.reset(ptr);
        } catch (...) {
          boost::checked_delete(ptr);
          fn(fd);
          throw;
        }
      }

      operator unspecified_bool_type() const {
        return get() == -1 ? 0 : &shared_fd::get;
      }

      bool operator!() const {
        return get() == -1;
      }

      int get() const {
        return ptr_ ? ptr_->get() : -1;
      }

      bool operator==(const shared_fd& rhs) const {
        return get() == rhs.get();
      }

      bool operator!=(const shared_fd& rhs) const {
        return get() != rhs.get();
      }

      bool operator<(const shared_fd& rhs) const {
        return get() < rhs.get();
      }

      void swap(shared_fd& rhs) {
        boost::swap(ptr_, rhs.ptr_);
      }

      void reset() {
        shared_fd().swap(*this);
      }

      void reset(int fd) {
        shared_fd(fd).swap(*this);
      }

      template <typename U>
      void reset(int fd, const U& fn) {
        shared_fd(fd, fn).swap(*this);
      }

      friend int get_fd(const shared_fd& self) {
        return self.get();
      }

      friend void swap(shared_fd& lhs, shared_fd& rhs) {
        lhs.swap(rhs);
      }

    private:
      boost::intrusive_ptr<shared_fd_count<T> > ptr_;
    };
  }

  typedef shared_fd_detail::shared_fd<boost::atomic<size_t> > shared_fd;
}

#endif
