#ifndef VAPOROID_SHARED_FD_SHARED_FD_COUNT_HPP
#define VAPOROID_SHARED_FD_SHARED_FD_COUNT_HPP

#include <boost/checked_delete.hpp>
#include <boost/noncopyable.hpp>

namespace vaporoid {
  namespace shared_fd_detail {
    template <typename T>
    class shared_fd_count : boost::noncopyable {
    public:
      explicit shared_fd_count(int fd)
        : count_(0), fd_(fd) {}

      virtual ~shared_fd_count() {}

      int get() const {
        return fd_;
      }

      friend void intrusive_ptr_add_ref(shared_fd_count* self) {
        ++self->count_;
      }

      friend void intrusive_ptr_release(shared_fd_count* self) {
        if (--self->count_ == 0) {
          boost::checked_delete(self);
        }
      }

    private:
      T count_;
      int fd_;
    };
  }
}

#endif
