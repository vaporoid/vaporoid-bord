#ifndef VAPOROID_LUA_ERROR_HPP
#define VAPOROID_LUA_ERROR_HPP

#include <sstream>
#include <stdexcept>
#include <string>
#include <lua.hpp>

namespace vaporoid {
  class lua_error : public std::runtime_error {
  public:
    explicit lua_error(const std::string& what, int code)
      : std::runtime_error(what), code_(code) {}

    virtual ~lua_error() throw() {}

    int code() const {
      return code_;
    }

    virtual const char* what() const throw() {
      try {
        if (what_.empty()) {
          std::ostringstream out;
          out << std::runtime_error::what() << ": ";

          switch (code_) {
            case LUA_ERRRUN:    out << "LUA_ERRRUN";    break;
            case LUA_ERRSYNTAX: out << "LUA_ERRSYNTAX"; break;
            case LUA_ERRMEM:    out << "LUA_ERRMEM";    break;
            case LUA_ERRGCMM:   out << "LUA_ERRGCMM";   break;
            case LUA_ERRERR:    out << "LUA_ERRERR";    break;
            case LUA_ERRFILE:   out << "LUA_ERRFILE";   break;
            default: out << "error number " << code_;
          }

          what_ = out.str();
        }
        return what_.c_str();
      } catch (...) {
        return std::runtime_error::what();
      }
    }

  private:
    int code_;
    mutable std::string what_;
  };
}

#endif
