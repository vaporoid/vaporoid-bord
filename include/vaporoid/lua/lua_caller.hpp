#ifndef VAPOROID_LUA_CALLER_HPP
#define VAPOROID_LUA_CALLER_HPP

#include <assert.h>
#include <string>
#include <boost/noncopyable.hpp>
#include <lua.hpp>

namespace vaporoid {
  class lua_caller : boost::noncopyable {
  public:
    explicit lua_caller(lua_State* state)
      : state_(state), argc_(), size_(-1) {}

    ~lua_caller() {
      clear();
    }

    template <typename T>
    void push_argument(const T& value) {
      push_(value);
      ++argc_;
    }

    template <typename T_head, typename... T_tail>
    int run(const T_head& head, const T_tail&... tail) {
      push_argument(head);
      return run(tail...);
    }

    int run() {
      int save = lua_gettop(state_) - argc_ - 1;
      int code = lua_pcall(state_, argc_, LUA_MULTRET, 0);
      size_ = lua_gettop(state_) - save;
      return code;
    }

    void clear() {
      if (size_ > 0) {
        lua_pop(state_, size_);
      }
      size_ = -1;
    }

    bool empty() {
      return size_ < 1;
    }

    int size() {
      return size_;
    }

    bool get_bool(int i) {
      assert(0 <= i && i < size_);
      return lua_toboolean(state_, i - size_);
    }

    double get_number(int i) {
      assert(0 <= i && i < size_);
      return lua_tonumber(state_, i - size_);
    }

    std::string get_string(int i) {
      assert(0 <= i && i < size_);
      size_t size = 0;
      const char* data = lua_tolstring(state_, i - size_, &size);
      return std::string(data, size);
    }

  private:
    lua_State* state_;
    int argc_;
    int size_;

    void push_(bool value) {
      lua_pushboolean(state_, value);
    }

    void push_(int value) {
      lua_pushinteger(state_, value);
    }

    void push_(double value) {
      lua_pushnumber(state_, value);
    }

    void push_(const char* value) {
      lua_pushstring(state_, value);
    }

    void push_(const std::string& value) {
      lua_pushlstring(state_, value.data(), value.size());
    }
  };
}

#endif
