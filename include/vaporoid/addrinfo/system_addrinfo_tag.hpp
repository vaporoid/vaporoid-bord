#ifndef VAPOROID_ADDRINFO_SYSTEM_ADDRINFO_TAG_HPP
#define VAPOROID_ADDRINFO_SYSTEM_ADDRINFO_TAG_HPP

#include <netdb.h>

namespace vaporoid {
  struct system_addrinfo_tag {
    typedef struct addrinfo type;

    enum {
      passive     = AI_PASSIVE,
      canonname   = AI_CANONNAME,
      numerichost = AI_NUMERICHOST,
      numericserv = AI_NUMERICSERV,
      v4mapped    = AI_V4MAPPED,
      all         = AI_ALL,
      addrconfig  = AI_ADDRCONFIG,
    };

    static const char* gai_strerror(int code) {
      return ::gai_strerror(code);
    }
  };
}

#endif
