#ifndef VAPOROID_ADDRINFO_ADDRINFO_ITERATOR_RANGE_HPP
#define VAPOROID_ADDRINFO_ADDRINFO_ITERATOR_RANGE_HPP

#include <boost/range/iterator_range.hpp>
#include <vaporoid/addrinfo/addrinfo_iterator.hpp>

namespace vaporoid {
  template <typename T>
  inline boost::iterator_range<addrinfo_iterator<T> > make_addrinfo_iterator_range(T* ptr) {
    return boost::make_iterator_range(addrinfo_iterator<T>(ptr), addrinfo_iterator<T>());
  }
}

#endif
