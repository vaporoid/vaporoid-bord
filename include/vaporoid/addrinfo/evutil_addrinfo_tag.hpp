#ifndef VAPOROID_ADDRINFO_EVUTIL_ADDRINFO_TAG_HPP
#define VAPOROID_ADDRINFO_EVUTIL_ADDRINFO_TAG_HPP

#include <event2/util.h>

namespace vaporoid {
  struct evutil_addrinfo_tag {
    typedef struct evutil_addrinfo type;

    enum {
      passive     = EVUTIL_AI_PASSIVE,
      canonname   = EVUTIL_AI_CANONNAME,
      numerichost = EVUTIL_AI_NUMERICHOST,
      numericserv = EVUTIL_AI_NUMERICSERV,
      v4mapped    = EVUTIL_AI_V4MAPPED,
      all         = EVUTIL_AI_ALL,
      addrconfig  = EVUTIL_AI_ADDRCONFIG,
    };

    static const char* gai_strerror(int code) {
      return evutil_gai_strerror(code);
    }
  };
}

#endif
