#ifndef VAPOROID_ADDRINFO_ADDRINFO_QUERY_HPP
#define VAPOROID_ADDRINFO_ADDRINFO_QUERY_HPP

#include <sys/socket.h>
#include <boost/optional.hpp>
#include <boost/utility/value_init.hpp>
#include <vaporoid/addrinfo/evutil_addrinfo_tag.hpp>
#include <vaporoid/addrinfo/system_addrinfo_tag.hpp>

namespace vaporoid {
  template <typename T_tag, typename T = typename T_tag::type>
  class addrinfo_query {
  public:
    typedef T_tag tag;

    addrinfo_query& ipv4() {
      return set_family_(AF_INET);
    }

    addrinfo_query& ipv6() {
      return set_family_(AF_INET6);
    }

    addrinfo_query& tcp() {
      return set_socktype_(SOCK_STREAM);
    }

    addrinfo_query& udp() {
      return set_socktype_(SOCK_DGRAM);
    }

    addrinfo_query& passive(bool value = true) {
      return set_flag_(T_tag::passive, value);
    }

    addrinfo_query& canonname(bool value = true) {
      return set_flag_(T_tag::canonname, value);
    }

    addrinfo_query& numerichost(bool value = true) {
      return set_flag_(T_tag::numerichost, value);
    }

    addrinfo_query& numericserv(bool value = true) {
      return set_flag_(T_tag::numericserv, value);
    }

    addrinfo_query& v4mapped(bool value = true) {
      return set_flag_(T_tag::v4mapped, value);
    }

    addrinfo_query& all(bool value = true) {
      return set_flag_(T_tag::all, value);
    }

    addrinfo_query& addrconfig(bool value = true) {
      return set_flag_(T_tag::addrconfig, value);
    }

    const T* get() const {
      return ai_.get_ptr();
    }

  private:
    boost::optional<T> ai_;

    T& set_() {
      if (!ai_) {
        ai_ = boost::initialized_value;
        ai_->ai_family = AF_UNSPEC;
      }
      return ai_.get();
    }

    addrinfo_query& set_family_(int family) {
      set_().ai_family = family;
      return *this;
    }

    addrinfo_query& set_socktype_(int socktype) {
      set_().ai_socktype = socktype;
      return *this;
    }

    addrinfo_query& set_flag_(int flag, bool value) {
      if (value) {
        set_().ai_flags |= flag;
      } else {
        set_().ai_flags &= ~flag;
      }
      return *this;
    }
  };

  typedef addrinfo_query<system_addrinfo_tag> system_addrinfo_query;
  typedef addrinfo_query<evutil_addrinfo_tag> evutil_addrinfo_query;
}

#endif
