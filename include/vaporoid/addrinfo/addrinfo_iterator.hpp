#ifndef VAPOROID_ADDRINFO_ADDRINFO_ITERATOR_HPP
#define VAPOROID_ADDRINFO_ADDRINFO_ITERATOR_HPP

#include <netdb.h>
#include <boost/iterator/iterator_facade.hpp>

namespace vaporoid {
  template <typename T>
  class addrinfo_iterator : public boost::iterator_facade<addrinfo_iterator<T>, T, boost::forward_traversal_tag> {
    friend class boost::iterator_core_access;
  public:
    explicit addrinfo_iterator(T* ptr = 0)
      : ptr_(ptr) {}

  private:
    T* ptr_;

    T& dereference() const {
      return *ptr_;
    }

    bool equal(const addrinfo_iterator& rhs) const {
      return ptr_ == rhs.ptr_;
    }

    void increment() {
      ptr_ = ptr_->ai_next;
    }
  };
}

#endif
