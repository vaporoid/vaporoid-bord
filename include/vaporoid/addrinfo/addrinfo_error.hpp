#ifndef VAPOROID_ADDRINFO_ADDRINFO_ERROR_HPP
#define VAPOROID_ADDRINFO_ADDRINFO_ERROR_HPP

#include <sstream>
#include <stdexcept>
#include <string>
#include <vaporoid/addrinfo/evutil_addrinfo_tag.hpp>
#include <vaporoid/addrinfo/system_addrinfo_tag.hpp>

namespace vaporoid {
  template <typename T_tag>
  class addrinfo_error : public std::runtime_error {
  public:
    typedef T_tag tag;

    explicit addrinfo_error(const std::string& what, int code)
      : std::runtime_error(what), code_(code) {}

    virtual ~addrinfo_error() throw() {}

    int code() const {
      return code_;
    }

    virtual const char* what() const throw() {
      try {
        if (what_.empty()) {
          std::ostringstream out;
          out << std::runtime_error::what() << ": ";

          if (const char* message = T_tag::gai_strerror(code_)) {
            out << message;
          } else {
            out << "error number " << code_;
          }

          what_ = out.str();
        }
        return what_.c_str();
      } catch (...) {
        return std::runtime_error::what();
      }
    }

  private:
    int code_;
    mutable std::string what_;
  };

  typedef addrinfo_error<system_addrinfo_tag> system_addrinfo_error;
  typedef addrinfo_error<evutil_addrinfo_tag> evutil_addrinfo_error;
}

#endif
