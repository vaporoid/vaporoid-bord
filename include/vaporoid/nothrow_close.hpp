#ifndef VAPOROID_NOTHROW_CLOSE_HPP
#define VAPOROID_NOTHROW_CLOSE_HPP

#include <unistd.h>
#include <vaporoid/unexpected.hpp>
#include <vaporoid/system_error.hpp>

namespace vaporoid {
  inline void nothrow_close(int fd) {
    if (close(fd) == -1) {
      VAPOROID_UNEXPECTED(system_error("could not close"));
    }
  }
}

#endif
