CREATE TABLE IF NOT EXISTS namespace (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  namespace TEXT NOT NULL UNIQUE,
  uri TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE,
  password TEXT,
  password_sha1 TEXT,
  phone TEXT NOT NULL UNIQUE,
  phone_url TEXT NOT NULL DEFAULT 'https://moveablefeast.jp/mfone/ak-phone.html',
  service_url TEXT NOT NULL DEFAULT 'https://moveablefeast.jp/mfone/ak-service.html',
  name TEXT NOT NULL,
  nickname TEXT NOT NULL DEFAULT '',
  note TEXT NOT NULL DEFAULT '',
  is_staff BOOLEAN NOT NULL DEFAULT false,
  namespace INTEGER REFERENCES namespace (id)
);

CREATE TABLE IF NOT EXISTS media_stream (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user INTEGER NOT NULL UNIQUE REFERENCES user (id),
  code TEXT NOT NULL UNIQUE, -- uuid
  fd INTEGER UNIQUE,
  host TEXT NULL,
  port INTEGER NULL,
  created TEXT NOT NULL,
  modified TEXT NOT NULL
);

INSERT INTO namespace (namespace, uri) VALUES ('AO', '');
INSERT INTO namespace (namespace, uri) VALUES ('AK', '');
