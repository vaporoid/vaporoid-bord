local ctx = vaporoid_bord.get_context()
local host, path = ...

ctx:set_debug(true)
ctx:set_nodelay(true)

ctx:setup_base()
ctx:setup_http(host, 4280)
ctx:setup_listener(nil, 4217)

function open_db ()
  return sqlite3.open(path)
end

function close_db (db)
  return db:close()
end

function prepare_statement (db, sql)
  return db:prepare(sql)
end

function finalize_statement (sth)
  sth:finalize()
end

function cb_media_stream (fd, host, port)
  local query = ctx:recv_query(fd)

  local command = query:get("command")
  if command == "REGISTER" then
    local db = open_db()
    local count = 0
    do
      local sth = prepare_statement(db, [[
        SELECT phone, phone_url, service_url FROM user WHERE username = ? AND password = ?
      ]])
      sth:bind(1, query:get("username"))
      sth:bind(2, query:get("password"))
      for i in sth:nrows() do
        count = count + 1
        query:set("phone", i.phone)
        query:set("phone_url", i.phone_url)
        query:set("service_url", i.service_url)
      end
      finalize_statement(sth)
    end
    if count == 1 then
      local sth = prepare_statement(db, [[
        REPLACE INTO connection (user, fd, host, port, created, modified) VALUES (
          (SELECT id FROM user WHERE username = ?),
          ?,
          ?,
          ?,
          datetime('now'),
          datetime('now')
        );
      ]])
      sth:bind(1, query:get("username"))
      sth:bind(2, fd)
      sth:bind(3, host)
      sth:bind(4, port)
      sth:step()
      finalize_statement(sth)
      query:set("result", "OK")
    else
      query:set("phone_url", "http://moveablefeast.jp:3080/mfone/start.html")
      query:set("service_url", "http://moveablefeast.jp:3080/mfone/no-service.html")
      query:set("result", "NG")
    end
    close_db(db)
  elseif command == "PING" then
    local db = open_db()
    local count = 0
    do
      local sth = prepare_statement(db, [[
        SELECT id FROM connection WHERE uuid = ?;
      ]])
      sth:bind(1, query:get("uuid"))
      for i in sth:nrows() do
        count = count + 1
      end
      finalize_statement(sth)
    end
    if count == 1 then
      local uuid = ctx:uuidgen()
      local sth = prepare_statement(db, [[
        UPDATE connection SET modified = datetime('now') WHERE uuid = ?;
      ]])
      sth:bind(1, uuid)
      sth:step()
      finalize_statement(sth)
      query:set("result", "OK")
    else
      query:set("result", "NG")
    end
    close_db(db)
  elseif command == "PING2" then
    print(command, query:get("code"))
    query:set("result", "OK")
  else
    query:set("result", "NG")
  end

  ctx:send_query(fd, query)
end

function cb_media_stream_close (fd)
  local db = open_db()
  local sth = prepare_statement(db, [[
    DELETE FROM connection WHERE fd = ?
  ]])
  sth:bind(1, fd)
  sth:step()
  finalize_statement(sth)
  close_db(db)
end

local function get_fd_by_phone (db, phone)
  local fd
  local count = 0

  local sth = prepare_statement(db, [[
    SELECT fd FROM connection WHERE user = (SELECT id FROM user WHERE phone = ?);
  ]])
  sth:bind(1, phone)
  for i in sth:nrows() do
    count = count + 1
    fd = i.fd
  end
  finalize_statement(sth)

  if count == 1 then
    return fd
  else
    return nil
  end
end

local function get_name_by_phone (db, phone)
  local name
  local nickname

  local sth = prepare_statement(db, [[
    SELECT name, nickname FROM user WHERE phone = ?;
  ]])
  sth:bind(1, phone)
  for i in sth:nrows() do
    name = i.name
    nickname = i.nickname
  end
  finalize_statement(sth)

  return name, nickname
end

function cb_http (uri)
  local result = "OK"

  print("uri", uri)

  if uri == "/shutdown" then
    ctx:shutdown()
  elseif uri == "/debug/true" then
    ctx:set_debug(true)
  elseif uri == "/debug/false" then
    ctx:set_debug(false)
  elseif uri == "/nodelay/true" then
    ctx:set_nodelay(true)
  elseif uri == "/nodelay/false" then
    ctx:set_nodelay(false)
  elseif uri:find("/start-rtp", 1, true) == 1 then
    local port = ctx:setup_rtp(host, 2000, 2100);
    local callback = uri:match("callback=([^&]+)")
    if callback then
      return string.format("%s({\"result\":\"OK\",\"host\":\"%s\",\"port\":%d})", callback, host, port), "application/json; charset=UTF-8"
    else
      return string.format("{\"result\":\"OK\",\"host\":\"%s\",\"port\":%d}", host, port), "application/json; charset=UTF-8"
    end
  elseif uri:find("/message", 1, true) == 1 then
    local query = ctx:http_query()
    local type = query:get("type")
    local from = query:get("from")
    local peer = query:get("peer")
    local text = query:get("text")

    local db = open_db()
    local count = 0
    local fd = get_fd_by_phone(db, peer)

    if fd then
      local q = vaporoid_bord.map_string_string()
      q:set("command", "MESSAGE")
      q:set("type", type)
      q:set("from", from)
      q:set("peer", peer)
      q:set("text", text)

      local from_name, from_nickname = get_name_by_phone(db, from)

      if from_name then
        q:set("from_name", from_name)
      end
      if from_nickname then
        q:set("from_nickname", from_nickname)
      end

      ctx:send_query(fd, q)
    else
      result = "NG"
    end
    close_db(db)

    if query:has_key("callback") then
      return query:get("callback") .. "({\"result\":\"" .. result .. "\"})", "application/json; charset=UTF-8"
    else
      return "{\"result\":\"" .. result .. "\"}", "application/json; charset=UTF-8"
    end
  elseif uri:find("/accept", 1, true) == 1 then
    local query = ctx:http_query()
    local from = query:get("from")
    local peer = query:get("peer")

    local port0 = ctx:setup_rtp(host, 2000, 2100)
    local port1 = ctx:setup_rtp(host, 3000, 3100)
    ctx:pair_rtp(port0, port1)
    ctx:pair_rtp(port0+1, port1+1)

    print(port0, port1)

    local db = open_db()
    do
      local fd   = get_fd_by_phone(db, from)
      local port = port0

      local q = vaporoid_bord.map_string_string()
      q:set("command", "CONNECT")
      q:set("host", host)
      q:set("port", tostring(port))
      ctx:send_query(fd, q)

      port0 = port
    end

    do
      local fd   = get_fd_by_phone(db, peer)
      local port = port1

      local q = vaporoid_bord.map_string_string()
      q:set("command", "CONNECT")
      q:set("host", host)
      q:set("port", tostring(port))
      ctx:send_query(fd, q)
    end

    close_db(db)

    if query:has_key("callback") then
      return query:get("callback") .. "({\"result\":\"" .. result .. "\"})", "application/json; charset=UTF-8"
    else
      return "{\"result\":\"" .. result .. "\"}", "application/json; charset=UTF-8"
    end
  elseif uri:find("/bye", 1, true) == 1 then
    local query = ctx:http_query()
    local from = query:get("from")
    local peer = query:get("peer")

    print(from, peer)

    local db = open_db()
    do
      local fd = get_fd_by_phone(db, from)

      print(fd)

      local q = vaporoid_bord.map_string_string()
      q:set("command", "DISCONNECT")
      ctx:send_query(fd, q)
    end

    do
      local fd = get_fd_by_phone(db, peer)

      print(fd)

      local q = vaporoid_bord.map_string_string()
      q:set("command", "DISCONNECT")
      ctx:send_query(fd, q)
    end

    close_db(db)

    if query:has_key("callback") then
      return query:get("callback") .. "({\"result\":\"" .. result .. "\"})", "application/json; charset=UTF-8"
    else
      return "{\"result\":\"" .. result .. "\"}", "application/json; charset=UTF-8"
    end

  elseif uri:find("/deny", 1, true) == 1 then
    local query = ctx:http_query()
    local from = query:get("from")
    local peer = query:get("peer")

    local db = open_db()
    do
      local fd = get_fd_by_phone(db, from)

      local q = vaporoid_bord.map_string_string()
      q:set("command", "DENY")
      ctx:send_query(fd, q)
    end
    close_db(db)

    if query:has_key("callback") then
      return query:get("callback") .. "({\"result\":\"" .. result .. "\"})", "application/json; charset=UTF-8"
    else
      return "{\"result\":\"" .. result .. "\"}", "application/json; charset=UTF-8"
    end
  end

  return "{\"result\":\"" .. result .. "\"}", "application/json; charset=UTF-8"
end
