#ifndef VAPOROID_BORD_MESSAGE_STREAM_HPP
#define VAPOROID_BORD_MESSAGE_STREAM_HPP

#include <cstdint>
#include <map>
#include <string>
#include <boost/noncopyable.hpp>
#include <msgpack.hpp>

namespace vaporoid_bord {
  class message_stream : boost::noncopyable {
  public:
    explicit message_stream(const std::string& host, std::uint16_t port)
      : host_(host), port_(port) {}

    const std::string& host() const {
      return host_;
    }

    std::uint16_t port() const {
      return port_;
    }

    msgpack::unpacker& unpacker() {
      return unpacker_;
    }

    void query(const std::map<std::string, std::string>& query) {
      query_ = query;
    }

    const std::map<std::string, std::string>& query() const {
      return query_;
    }

  private:
    std::string host_;
    std::uint16_t port_;
    msgpack::unpacker unpacker_;
    std::map<std::string, std::string> query_;
  };
}

#endif
