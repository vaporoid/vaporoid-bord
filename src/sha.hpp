#ifndef VAPOROID_SHA_HPP
#define VAPOROID_SHA_HPP

#include <stddef.h>
#include <string.h>
#include <algorithm>
#include <boost/noncopyable.hpp>
#include "stdint.hpp"
#include "word.hpp"

namespace vaporoid {
  namespace sha_detail {
    template <typename T>
    class dword {
    public:
      explicit dword()
        : upper_(), lower_() {}

      dword& operator+=(T rhs) {
        T lower = lower_;
        lower_ += rhs;
        if (lower_ < lower) {
          ++upper_;
        }
        return *this;
      }

      dword& operator<<=(size_t rhs) {
        upper_ <<= rhs;
        upper_ |= lower_ >> (sizeof(T) * 8 - rhs);
        lower_ <<= rhs;
        return *this;
      }

      T upper() const {
        return upper_;
      }

      T lower() const {
        return lower_;
      }

    private:
      T upper_;
      T lower_;
    };

    template <typename T>
    inline T dword_upper(const dword<T>& value) {
      return value.upper();
    }

    template <typename T>
    inline T dword_lower(const dword<T>& value) {
      return value.lower();
    }

    inline uint32_t dword_upper(uint64_t value) {
      return value >> 32;
    }

    inline uint32_t dword_lower(uint64_t value) {
      return value;
    }

    template <typename T, typename T_dword, size_t T_block_size_bits>
    class block : boost::noncopyable {
    public:
      static const size_t BLOCK_SIZE_BITS    = T_block_size_bits;
      static const size_t BLOCK_SIZE         = BLOCK_SIZE_BITS / 8;
      static const size_t BLOCK_PADDING_SIZE = BLOCK_SIZE - sizeof(T) * 2;

      explicit block()
        : copy_(), i_(), size_() {}

      bool full() const {
        return i_ == 0;
      }

      T operator[](size_t i) const {
        return byte_to_word<T>(copy_ + sizeof(T) * i);
      }

      void reset() {
        i_    = 0;
        size_ = T_dword();
      }

      const uint8_t* update(const uint8_t* i, const uint8_t* end) {
        if (i_ == 0) {
          if (i + BLOCK_SIZE <= end) {
            copy_ = i;
            size_ += BLOCK_SIZE;
            return i + BLOCK_SIZE;
          }
          copy_ = data_;
        }

        size_t size = std::min<size_t>(BLOCK_SIZE - i_, end - i);
        memmove(data_ + i_, i, size);
        i_ += size;
        if (i_ == BLOCK_SIZE) {
          i_ = 0;
        }
        size_ += size;
        return i + size;
      }

      bool close() {
        copy_ = data_;
        data_[i_] = 0x80;
        ++i_;
        if (i_ > BLOCK_PADDING_SIZE) {
          memset(data_ + i_, 0, BLOCK_SIZE - i_);
          i_ = 0;
          return true;
        } else {
          memset(data_ + i_, 0, BLOCK_PADDING_SIZE - i_);
          return false;
        }
      }

      void finalize() {
        memset(data_ + i_, 0, BLOCK_PADDING_SIZE - i_);
        size_ <<= 3;
        word_to_byte(dword_upper(size_), data_ + BLOCK_PADDING_SIZE);
        word_to_byte(dword_lower(size_), data_ + BLOCK_PADDING_SIZE + sizeof(T));
        i_ = BLOCK_SIZE;
      }

    private:
      uint8_t        data_[BLOCK_SIZE];
      const uint8_t* copy_;
      size_t         i_;
      T_dword        size_;
    };

    template <typename T>
    inline T ROTL(size_t n, T x) {
      return x << n | x >> (sizeof(T) * 8 - n);
    }

    template <typename T>
    inline T ROTR(size_t n, T x) {
      return x >> n | x << (sizeof(T) * 8 - n);
    }

    template <typename T>
    inline T SHR(size_t n, T x) {
      return x >> n;
    }

    template <typename T>
    inline T Ch(T x, T y, T z) {
      return (x & y) ^ (~x & z);
    }

    template <typename T>
    inline T Parity(T x, T y, T z) {
      return x ^ y ^ z;
    }

    template <typename T>
    inline T Maj(T x, T y, T z) {
      return (x & y) ^ (x & z) ^ (y & z);
    }

    template <typename T>
    inline T sum256_0(T x) {
      return ROTR(2, x) ^ ROTR(13, x) ^ ROTR(22, x);
    }

    template <typename T>
    inline T sum256_1(T x) {
      return ROTR(6, x) ^ ROTR(11, x) ^ ROTR(25, x);
    }

    template <typename T>
    inline T sigma256_0(T x) {
      return ROTR(7, x) ^ ROTR(18, x) ^ SHR(3, x);
    }

    template <typename T>
    inline T sigma256_1(T x) {
      return ROTR(17, x) ^ ROTR(19, x) ^ SHR(10, x);
    }

    template <typename T>
    inline T sum512_0(T x) {
      return ROTR(28, x) ^ ROTR(34, x) ^ ROTR(39, x);
    }

    template <typename T>
    inline T sum512_1(T x) {
      return ROTR(14, x) ^ ROTR(18, x) ^ ROTR(41, x);
    }

    template <typename T>
    inline T sigma512_0(T x) {
      return ROTR(1, x) ^ ROTR(8, x) ^ SHR(7, x);
    }

    template <typename T>
    inline T sigma512_1(T x) {
      return ROTR(19, x) ^ ROTR(61, x) ^ SHR(6, x);
    }

    static const uint32_t K_0 = 0x5a827999UL;
    static const uint32_t K_1 = 0x6ed9eba1UL;
    static const uint32_t K_2 = 0x8f1bbcdcUL;
    static const uint32_t K_3 = 0xca62c1d6UL;

    inline uint32_t K256(size_t t) {
      static const uint32_t DATA[] = {
        0x428a2f98UL, 0x71374491UL, 0xb5c0fbcfUL, 0xe9b5dba5UL,
        0x3956c25bUL, 0x59f111f1UL, 0x923f82a4UL, 0xab1c5ed5UL,
        0xd807aa98UL, 0x12835b01UL, 0x243185beUL, 0x550c7dc3UL,
        0x72be5d74UL, 0x80deb1feUL, 0x9bdc06a7UL, 0xc19bf174UL,
        0xe49b69c1UL, 0xefbe4786UL, 0x0fc19dc6UL, 0x240ca1ccUL,
        0x2de92c6fUL, 0x4a7484aaUL, 0x5cb0a9dcUL, 0x76f988daUL,
        0x983e5152UL, 0xa831c66dUL, 0xb00327c8UL, 0xbf597fc7UL,
        0xc6e00bf3UL, 0xd5a79147UL, 0x06ca6351UL, 0x14292967UL,
        0x27b70a85UL, 0x2e1b2138UL, 0x4d2c6dfcUL, 0x53380d13UL,
        0x650a7354UL, 0x766a0abbUL, 0x81c2c92eUL, 0x92722c85UL,
        0xa2bfe8a1UL, 0xa81a664bUL, 0xc24b8b70UL, 0xc76c51a3UL,
        0xd192e819UL, 0xd6990624UL, 0xf40e3585UL, 0x106aa070UL,
        0x19a4c116UL, 0x1e376c08UL, 0x2748774cUL, 0x34b0bcb5UL,
        0x391c0cb3UL, 0x4ed8aa4aUL, 0x5b9cca4fUL, 0x682e6ff3UL,
        0x748f82eeUL, 0x78a5636fUL, 0x84c87814UL, 0x8cc70208UL,
        0x90befffaUL, 0xa4506cebUL, 0xbef9a3f7UL, 0xc67178f2UL,
      };
      return DATA[t];
    }

    inline uint64_t K512(size_t t) {
      static const uint64_t DATA[] = {
        0x428a2f98d728ae22ULL, 0x7137449123ef65cdULL, 0xb5c0fbcfec4d3b2fULL, 0xe9b5dba58189dbbcULL,
        0x3956c25bf348b538ULL, 0x59f111f1b605d019ULL, 0x923f82a4af194f9bULL, 0xab1c5ed5da6d8118ULL,
        0xd807aa98a3030242ULL, 0x12835b0145706fbeULL, 0x243185be4ee4b28cULL, 0x550c7dc3d5ffb4e2ULL,
        0x72be5d74f27b896fULL, 0x80deb1fe3b1696b1ULL, 0x9bdc06a725c71235ULL, 0xc19bf174cf692694ULL,
        0xe49b69c19ef14ad2ULL, 0xefbe4786384f25e3ULL, 0x0fc19dc68b8cd5b5ULL, 0x240ca1cc77ac9c65ULL,
        0x2de92c6f592b0275ULL, 0x4a7484aa6ea6e483ULL, 0x5cb0a9dcbd41fbd4ULL, 0x76f988da831153b5ULL,
        0x983e5152ee66dfabULL, 0xa831c66d2db43210ULL, 0xb00327c898fb213fULL, 0xbf597fc7beef0ee4ULL,
        0xc6e00bf33da88fc2ULL, 0xd5a79147930aa725ULL, 0x06ca6351e003826fULL, 0x142929670a0e6e70ULL,
        0x27b70a8546d22ffcULL, 0x2e1b21385c26c926ULL, 0x4d2c6dfc5ac42aedULL, 0x53380d139d95b3dfULL,
        0x650a73548baf63deULL, 0x766a0abb3c77b2a8ULL, 0x81c2c92e47edaee6ULL, 0x92722c851482353bULL,
        0xa2bfe8a14cf10364ULL, 0xa81a664bbc423001ULL, 0xc24b8b70d0f89791ULL, 0xc76c51a30654be30ULL,
        0xd192e819d6ef5218ULL, 0xd69906245565a910ULL, 0xf40e35855771202aULL, 0x106aa07032bbd1b8ULL,
        0x19a4c116b8d2d0c8ULL, 0x1e376c085141ab53ULL, 0x2748774cdf8eeb99ULL, 0x34b0bcb5e19b48a8ULL,
        0x391c0cb3c5c95a63ULL, 0x4ed8aa4ae3418acbULL, 0x5b9cca4f7763e373ULL, 0x682e6ff3d6b2b8a3ULL,
        0x748f82ee5defb2fcULL, 0x78a5636f43172f60ULL, 0x84c87814a1f0ab72ULL, 0x8cc702081a6439ecULL,
        0x90befffa23631e28ULL, 0xa4506cebde82bde9ULL, 0xbef9a3f7b2c67915ULL, 0xc67178f2e372532bULL,
        0xca273eceea26619cULL, 0xd186b8c721c0c207ULL, 0xeada7dd6cde0eb1eULL, 0xf57d4f7fee6ed178ULL,
        0x06f067aa72176fbaULL, 0x0a637dc5a2c898a6ULL, 0x113f9804bef90daeULL, 0x1b710b35131c471bULL,
        0x28db77f523047d84ULL, 0x32caab7b40c72493ULL, 0x3c9ebe0a15c9bebcULL, 0x431d67c49c100d4cULL,
        0x4cc5d4becb3e42b6ULL, 0x597f299cfc657e2aULL, 0x5fcb6fab3ad6faecULL, 0x6c44198c4a475817ULL,
      };
      return DATA[t];
    }

    struct sha1_trait {
      static const size_t MESSAGE_DIGEST_SIZE_BITS = 160;

      template <typename T>
      static void reset(T& H) {
        H[0] = 0x67452301UL;
        H[1] = 0xefcdab89UL;
        H[2] = 0x98badcfeUL;
        H[3] = 0x10325476UL;
        H[4] = 0xc3d2e1f0UL;
      }

      template <typename T>
      static uint8_t* finalize(const T& H, uint8_t* i) {
        i = word_to_byte(H[0], i);
        i = word_to_byte(H[1], i);
        i = word_to_byte(H[2], i);
        i = word_to_byte(H[3], i);
        i = word_to_byte(H[4], i);
        return i;
      }
    };

    struct sha224_trait {
      static const size_t MESSAGE_DIGEST_SIZE_BITS = 224;

      template <typename T>
      static void reset(T& H) {
        H[0] = 0xc1059ed8UL;
        H[1] = 0x367cd507UL;
        H[2] = 0x3070dd17UL;
        H[3] = 0xf70e5939UL;
        H[4] = 0xffc00b31UL;
        H[5] = 0x68581511UL;
        H[6] = 0x64f98fa7UL;
        H[7] = 0xbefa4fa4UL;
      }

      template <typename T>
      static uint8_t* finalize(const T& H, uint8_t* i) {
        i = word_to_byte(H[0], i);
        i = word_to_byte(H[1], i);
        i = word_to_byte(H[2], i);
        i = word_to_byte(H[3], i);
        i = word_to_byte(H[4], i);
        i = word_to_byte(H[5], i);
        i = word_to_byte(H[6], i);
        return i;
      }
    };

    struct sha256_trait {
      static const size_t MESSAGE_DIGEST_SIZE_BITS = 256;

      template <typename T>
      static void reset(T& H) {
        H[0] = 0x6a09e667UL;
        H[1] = 0xbb67ae85UL;
        H[2] = 0x3c6ef372UL;
        H[3] = 0xa54ff53aUL;
        H[4] = 0x510e527fUL;
        H[5] = 0x9b05688cUL;
        H[6] = 0x1f83d9abUL;
        H[7] = 0x5be0cd19UL;
      }

      template <typename T>
      static uint8_t* finalize(const T& H, uint8_t* i) {
        i = word_to_byte(H[0], i);
        i = word_to_byte(H[1], i);
        i = word_to_byte(H[2], i);
        i = word_to_byte(H[3], i);
        i = word_to_byte(H[4], i);
        i = word_to_byte(H[5], i);
        i = word_to_byte(H[6], i);
        i = word_to_byte(H[7], i);
        return i;
      }
    };

    struct sha384_trait {
      static const size_t MESSAGE_DIGEST_SIZE_BITS = 384;

      template <typename T>
      static void reset(T& H) {
        H[0] = 0xcbbb9d5dc1059ed8ULL;
        H[1] = 0x629a292a367cd507ULL;
        H[2] = 0x9159015a3070dd17ULL;
        H[3] = 0x152fecd8f70e5939ULL;
        H[4] = 0x67332667ffc00b31ULL;
        H[5] = 0x8eb44a8768581511ULL;
        H[6] = 0xdb0c2e0d64f98fa7ULL;
        H[7] = 0x47b5481dbefa4fa4ULL;
      }

      template <typename T>
      static uint8_t* finalize(const T& H, uint8_t* i) {
        i = word_to_byte(H[0], i);
        i = word_to_byte(H[1], i);
        i = word_to_byte(H[2], i);
        i = word_to_byte(H[3], i);
        i = word_to_byte(H[4], i);
        i = word_to_byte(H[5], i);
        return i;
      }
    };

    struct sha512_trait {
      static const size_t MESSAGE_DIGEST_SIZE_BITS = 512;

      template <typename T>
      static void reset(T& H) {
        H[0] = 0x6a09e667f3bcc908ULL;
        H[1] = 0xbb67ae8584caa73bULL;
        H[2] = 0x3c6ef372fe94f82bULL;
        H[3] = 0xa54ff53a5f1d36f1ULL;
        H[4] = 0x510e527fade682d1ULL;
        H[5] = 0x9b05688c2b3e6c1fULL;
        H[6] = 0x1f83d9abfb41bd6bULL;
        H[7] = 0x5be0cd19137e2179ULL;
      }

      template <typename T>
      static uint8_t* finalize(const T& H, uint8_t* i) {
        i = word_to_byte(H[0], i);
        i = word_to_byte(H[1], i);
        i = word_to_byte(H[2], i);
        i = word_to_byte(H[3], i);
        i = word_to_byte(H[4], i);
        i = word_to_byte(H[5], i);
        i = word_to_byte(H[6], i);
        i = word_to_byte(H[7], i);
        return i;
      }
    };

    template <typename T_trait>
    struct sha1_algorithm {
      typedef T_trait trait_type;

      block<uint32_t, uint64_t, 512> M;
      uint32_t                       W[80];
      uint32_t                       H[8];

      void update() {
        for (int t = 0; t < 16; ++t) {
          W[t] = M[t];
        }

        for (int t = 16; t < 80; ++t) {
          W[t] = ROTL(1, W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16]);
        }

        uint32_t a = H[0];
        uint32_t b = H[1];
        uint32_t c = H[2];
        uint32_t d = H[3];
        uint32_t e = H[4];

        for (int t = 0; t < 20; ++t) {
          uint32_t T = ROTL(5, a) + Ch(b, c, d) + e + K_0 + W[t];
          e = d;
          d = c;
          c = ROTL(30, b);
          b = a;
          a = T;
        }

        for (int t = 20; t < 40; ++t) {
          uint32_t T = ROTL(5, a) + Parity(b, c, d) + e + K_1 + W[t];
          e = d;
          d = c;
          c = ROTL(30, b);
          b = a;
          a = T;
        }

        for (int t = 40; t < 60; ++t) {
          uint32_t T = ROTL(5, a) + Maj(b, c, d) + e + K_2 + W[t];
          e = d;
          d = c;
          c = ROTL(30, b);
          b = a;
          a = T;
        }

        for (int t = 60; t < 80; ++t) {
          uint32_t T = ROTL(5, a) + Parity(b, c, d) + e + K_3 + W[t];
          e = d;
          d = c;
          c = ROTL(30, b);
          b = a;
          a = T;
        }

        H[0] += a;
        H[1] += b;
        H[2] += c;
        H[3] += d;
        H[4] += e;
      }
    };

    template <typename T_trait>
    struct sha256_algorithm {
      typedef T_trait trait_type;

      block<uint32_t, uint64_t, 512> M;
      uint32_t                       W[64];
      uint32_t                       H[8];

      void update() {
        for (int t = 0; t < 16; ++t) {
          W[t] = M[t];
        }

        for (int t = 16; t < 64; ++t) {
          W[t] = sigma256_1(W[t - 2]) + W[t - 7] + sigma256_0(W[t - 15]) + W[t - 16];
        }

        uint32_t a = H[0];
        uint32_t b = H[1];
        uint32_t c = H[2];
        uint32_t d = H[3];
        uint32_t e = H[4];
        uint32_t f = H[5];
        uint32_t g = H[6];
        uint32_t h = H[7];

        for (int t = 0; t < 64; ++t) {
          uint32_t T1 = h + sum256_1(e) + Ch(e, f, g) + K256(t) + W[t];
          uint32_t T2 = sum256_0(a) + Maj(a, b, c);
          h = g;
          g = f;
          f = e;
          e = d + T1;
          d = c;
          c = b;
          b = a;
          a = T1 + T2;
        }

        H[0] += a;
        H[1] += b;
        H[2] += c;
        H[3] += d;
        H[4] += e;
        H[5] += f;
        H[6] += g;
        H[7] += h;
      }
    };

    template <typename T>
    struct sha512_algorithm {
      typedef T trait_type;

      block<uint64_t, dword<uint64_t>, 1024> M;
      uint64_t                               W[80];
      uint64_t                               H[8];

      void update() {
        for (int t = 0; t < 16; ++t) {
          W[t] = M[t];
        }

        for (int t = 16; t < 80; ++t) {
          W[t] = sigma512_1(W[t - 2]) + W[t - 7] + sigma512_0(W[t - 15]) + W[t - 16];
        }

        uint64_t a = H[0];
        uint64_t b = H[1];
        uint64_t c = H[2];
        uint64_t d = H[3];
        uint64_t e = H[4];
        uint64_t f = H[5];
        uint64_t g = H[6];
        uint64_t h = H[7];

        for (int t = 0; t < 80; ++t) {
          uint64_t T1 = h + sum512_1(e) + Ch(e, f, g) + K512(t) + W[t];
          uint64_t T2 = sum512_0(a) + Maj(a, b, c);
          h = g;
          g = f;
          f = e;
          e = d + T1;
          d = c;
          c = b;
          b = a;
          a = T1 + T2;
        }

        H[0] += a;
        H[1] += b;
        H[2] += c;
        H[3] += d;
        H[4] += e;
        H[5] += f;
        H[6] += g;
        H[7] += h;
      }
    };

    template <typename T_algorithm>
    class context {
    public:
      static const size_t MESSAGE_DIGEST_SIZE_BITS = T_algorithm::trait_type::MESSAGE_DIGEST_SIZE_BITS;
      static const size_t MESSAGE_DIGEST_SIZE      = MESSAGE_DIGEST_SIZE_BITS / 8;

      explicit context() {
        T_algorithm::trait_type::reset(algorithm_.H);
      }

      void reset() {
        algorithm_.M.reset();
        T_algorithm::trait_type::reset(algorithm_.H);
      }

      void update(const uint8_t* i, const uint8_t* end) {
        while (i != end) {
          i = algorithm_.M.update(i, end);
          if (algorithm_.M.full()) {
            algorithm_.update();
          }
        }
      }

      void update(const void* data, size_t size) {
        update(static_cast<const uint8_t*>(data), static_cast<const uint8_t*>(data) + size);
      }

      uint8_t* finalize(uint8_t* i) {
        if (algorithm_.M.close()) {
          algorithm_.update();
        }
        algorithm_.M.finalize();
        algorithm_.update();
        return T_algorithm::trait_type::finalize(algorithm_.H, i);
      }

    private:
      T_algorithm algorithm_;
    };

    typedef context<sha1_algorithm  <sha1_trait>   > sha1;
    typedef context<sha256_algorithm<sha224_trait> > sha224;
    typedef context<sha256_algorithm<sha256_trait> > sha256;
    typedef context<sha512_algorithm<sha384_trait> > sha384;
    typedef context<sha512_algorithm<sha512_trait> > sha512;
  }

  using sha_detail::sha1;
  using sha_detail::sha224;
  using sha_detail::sha256;
  using sha_detail::sha384;
  using sha_detail::sha512;
}

#endif
