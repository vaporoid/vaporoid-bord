#ifndef VAPOROID_DICTINONARY_HPP
#define VAPOROID_DICTINONARY_HPP

#include <map>
#include <string>
#include <boost/optional.hpp>

namespace vaporoid {
  typedef std::map<std::string, std::string> dictionary;

  inline boost::optional<std::string> get(const dictionary& self, const std::string& key) {
    const dictionary::const_iterator i = self.find(key);
    if (i == self.end()) {
      return boost::optional<std::string>();
    } else {
      return boost::optional<std::string>(i->second);
    }
  }
}

#endif
