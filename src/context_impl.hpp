#ifndef VAPOROID_BORD_CONTEXT_IMPL_HPP
#define VAPOROID_BORD_CONTEXT_IMPL_HPP

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>

#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/event.h>
#include <event2/http.h>
#include <event2/listener.h>

#include <iostream>
#include <map>
#include <set>
#include <boost/current_function.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/tuple/tuple.hpp>
#include <lua.hpp>

#include <vaporoid/evutil_socket_error.hpp>
#include <vaporoid/lua.hpp>
#include <vaporoid/query.hpp>
#include <vaporoid/shared_fd.hpp>
#include <vaporoid/system_error.hpp>
#include "context.hpp"

#include "message_stream.hpp"

namespace vaporoid_bord {

  struct item {
//     explicit item() {
//       port = 0;
//       memset(&sas, 0, sizeof(sas));
//       socklen = 0;
//     }

//    explicit item() {
//      port = 0;
//      memset(&sas, 0, sizeof(sas));
//      socklen = 0;
//      peer_fd = -1;
//    }

    vaporoid::shared_fd fd;
    vaporoid::shared_fd pair_fd;
    boost::shared_ptr<struct event> ev;
    std::string host;
    uint16_t port;
    struct sockaddr_storage sas;
    socklen_t socklen;

    int self_port;
    int peer_port;
    int peer_fd;
  };

  inline std::pair<std::string, uint16_t> host_port(const struct sockaddr* address, socklen_t) {
      switch (address->sa_family) {
        case AF_INET:
          {
            const struct sockaddr_in* sin = reinterpret_cast<const struct sockaddr_in*>(address);
            char buffer[INET_ADDRSTRLEN] = { 0 };
            if (!inet_ntop(AF_INET, &sin->sin_addr, buffer, INET_ADDRSTRLEN)) {
              throw vaporoid::system_error("could not inet_ntop");
            }
            return std::make_pair(buffer, ntohs(sin->sin_port));
          }
          break;
        case AF_INET6:
          {
            const struct sockaddr_in6* sin6 = reinterpret_cast<const struct sockaddr_in6*>(address);
            char buffer[INET6_ADDRSTRLEN] = { 0 };
            if (!inet_ntop(AF_INET6, &sin6->sin6_addr, buffer, INET6_ADDRSTRLEN)) {
              throw vaporoid::system_error("could not inet_ntop");
            }
            return std::make_pair(buffer, ntohs(sin6->sin6_port));
          }
          break;
        default:
          return std::make_pair(std::string(), 0);
      }
  }

  class context::impl {
  public:
    explicit impl()
      : debug_(), nodelay_() {}

    bool                                     debug_;
    bool                                     nodelay_;
    boost::shared_ptr<lua_State>             state_;
    boost::shared_ptr<struct event_base>     base_;
    boost::shared_ptr<struct evhttp>         http_;
    boost::shared_ptr<struct evconnlistener> listener_;
    std::set<struct bufferevent*>            bev_;
    std::map<int, item>                      map_;

    std::map<int, boost::shared_ptr<message_stream> > message_stream_;
    std::map<std::string, std::string> http_query_;

    static void cb_http(struct evhttp_request* request, void* ctx) {
      static_cast<impl*>(ctx)->impl_cb_http(request);
    }

    static void cb_accept(struct evconnlistener* listener, int fd, struct sockaddr* address, int size, void* ctx) {
      static_cast<impl*>(ctx)->impl_cb_accept(listener, fd, address, size);
    }

    static void cb_accept_error(struct evconnlistener* listener, void* ctx) {
      static_cast<impl*>(ctx)->impl_cb_accept_error(listener);
    }

    static void cb_event_read(struct bufferevent* bev, void* ctx) {
      static_cast<impl*>(ctx)->impl_cb_event_read(bev);
    }

    static void cb_event(struct bufferevent* bev, short event, void* ctx) {
      static_cast<impl*>(ctx)->impl_cb_event(bev, event);
    }

    static void cb_udp(int fd, short event, void* ctx) {
      static_cast<impl*>(ctx)->impl_cb_udp(fd, event);
    }

  private:
    void impl_cb_http(struct evhttp_request* request) {
      lua_State* L = state_.get();
      lua_getglobal(L, "cb_http");

      const struct evhttp_uri* uri = evhttp_request_get_evhttp_uri(request);
      const char* q = evhttp_uri_get_query(uri);
      http_query_.clear();
      if (q) {
        http_query_ = vaporoid::parse_query(q);
      }

      vaporoid::lua_caller caller(L);
      int code = caller.run(evhttp_request_get_uri(request));
      if (code == LUA_OK) {
        boost::shared_ptr<struct evbuffer> buffer(evbuffer_new(), evbuffer_free);
        if (!buffer) {
          throw std::runtime_error("could not evbuffer_new");
        }
        if (caller.size() > 0) {
          std::string content = caller.get_string(0);
          if (evbuffer_add(buffer.get(), content.data(), content.size()) == -1) {
            throw std::runtime_error("could not evbuffer_add");
          }
        }
        if (caller.size() > 1) {
          struct evkeyvalq* header = evhttp_request_get_output_headers(request);
          evhttp_add_header(header, "Content-Type", caller.get_string(1).c_str());
        }
        evhttp_send_reply(request, HTTP_OK, "OK", buffer.get());
      } else {
        if (!caller.empty()) {
          std::cerr << "[ERROR] " << caller.get_string(0) << "\n";
        }
        std::cerr << "[ERROR] " << vaporoid::lua_error("could not lua_pcall", code).what() << "\n";
        evhttp_send_error(request, HTTP_INTERNAL, "Server Error");
      }
    }

    void impl_cb_accept(struct evconnlistener* listener, int fd, struct sockaddr* address, int size) {
      if (debug_) {
        std::cout << BOOST_CURRENT_FUNCTION << "\n";
      }

      std::string host;
      uint16_t port = 0;
      boost::tie(host, port) = host_port(address, size);

      if (debug_) {
        std::cout << "host: " << host << "\n";
        std::cout << "port: " << port << "\n";
      }

      int nodelay = nodelay_;
      setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &nodelay, sizeof(nodelay));

      struct bufferevent* bev = bufferevent_socket_new(evconnlistener_get_base(listener), fd, BEV_OPT_CLOSE_ON_FREE);
      if (!bev) {
        throw std::runtime_error("could not bufferevent_socket_new");
      }
      bev_.insert(bev);

      message_stream_.insert(
          std::make_pair(
              fd,
              boost::make_shared<message_stream>(host, port)));

      bufferevent_setcb(bev, &impl::cb_event_read, 0, &impl::cb_event, this);
      bufferevent_enable(bev, EV_READ | EV_WRITE);
    }

    void impl_cb_accept_error(struct evconnlistener*) {
      throw vaporoid::evutil_socket_error("cb_accept_error");
    }

    void impl_cb_event_read(struct bufferevent* bev) {
      if (debug_) {
        std::cout << BOOST_CURRENT_FUNCTION << "\n";
      }

      struct evbuffer* input = bufferevent_get_input(bev);
      size_t size = evbuffer_get_length(input);
      if (size > 0) {
        boost::shared_ptr<message_stream> message_stream = message_stream_[bufferevent_getfd(bev)];
        msgpack::unpacker& unpacker = message_stream->unpacker();
        unpacker.reserve_buffer(size);
        ssize_t copy_size = evbuffer_copyout(input, unpacker.buffer(), size);
        unpacker.buffer_consumed(copy_size);
        evbuffer_drain(input, copy_size);

        msgpack::unpacked result;
        while (unpacker.next(&result)) {
          msgpack::object object = result.get();
          std::map<std::string, std::string> query;
          object.convert(&query);
          message_stream->query(query);

          lua_State* L = state_.get();
          lua_getglobal(L, "cb_media_stream");
          vaporoid::lua_caller caller(L);
          int code = caller.run(
              bufferevent_getfd(bev),
              message_stream->host(),
              message_stream->port());
          if (code != LUA_OK) {
            if (!caller.empty()) {
              std::cerr << "[ERROR] " << caller.get_string(0) << "\n";
            }
            std::cerr << "[ERROR] " << vaporoid::lua_error("could not lua_pcall", code).what() << "\n";
          }
        }
      }

/*
      struct bufferevent* peer_bev = 0;
      BOOST_FOREACH(struct bufferevent* i, bev_) {
        if (bev != i) {
          peer_bev = i;
          break;
        }
      }

      if (peer_bev) {
        struct evbuffer* input = bufferevent_get_input(bev);
        struct evbuffer* output = bufferevent_get_output(peer_bev);
        evbuffer_add_buffer(output, input);
      } else {
        struct evbuffer* input = bufferevent_get_input(bev);
        size_t size = evbuffer_get_length(input);
        if (size > 0) {
          evbuffer_drain(input, size);
        }
      }
*/
    }

    void impl_cb_event(struct bufferevent* bev, short event) {
      if (debug_) {
        std::cout << BOOST_CURRENT_FUNCTION << "\n";
      }

      if (event & BEV_EVENT_ERROR) {
        std::cerr << "[ERROR] cb_event\n";
      }
      if (event & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) {
        lua_State* L = state_.get();
        lua_getglobal(L, "cb_media_stream_close");
        vaporoid::lua_caller caller(L);
        int code = caller.run(bufferevent_getfd(bev));
        if (code != LUA_OK) {
          if (!caller.empty()) {
            std::cerr << "[ERROR] " << caller.get_string(0) << "\n";
          }
          std::cerr << "[ERROR] " << vaporoid::lua_error("could not lua_pcall", code).what() << "\n";
        }

        message_stream_.erase(bufferevent_getfd(bev));
        bev_.erase(bev);
        bufferevent_free(bev);
      }
    }

    void impl_cb_udp(int fd, short event) {
      // if (debug_) {
      //   std::cout << BOOST_CURRENT_FUNCTION << "\n";
      //   std::cout << "event: " << event << "\n";
      // }

      if (event & EV_READ) {
        struct sockaddr_storage sas;
        memset(&sas, 0, sizeof(sas));
        socklen_t socklen = sizeof(sas);

        char buffer[4096] = { 0 };
        ssize_t size = recvfrom(fd, buffer, 4096, 0, reinterpret_cast<struct sockaddr*>(&sas), &socklen);

        std::string host;
        uint16_t port;

        item& x = map_[fd];
        if (x.host.empty()) {
          boost::tie(host, port) = host_port(reinterpret_cast<struct sockaddr*>(&sas), socklen);
          x.host = host;
          x.port = port;
          x.sas = sas;
          x.socklen = socklen;
        } else {
          // check host and port
          host = x.host;
          port = x.port;
        }

        // if (debug_) {
        //   std::cout << "host: " << host << "\n";
        //   std::cout << "port: " << port << "\n";
        //   std::cout << "size: " << size << "\n";
        // }

        // buffer[8] = buffer[8] + 17;
        // buffer[9] = buffer[9] + 42;

/*
        const item* peer = 0;
        typedef std::pair<int, item> pair_type;
        BOOST_FOREACH(const pair_type& i, map_) {
          if (i.second.port > 0 && i.second.port % 2 == 0 && i.second.fd.get() != fd) {
            peer = &i.second;
            break;
          }
        }

        if (debug_) {
          std::cout << "peer: " << peer << "\n";
          if (peer) {
            std::cout << "peer-host: " << peer->host << "\n";
            std::cout << "peer-port: " << peer->port << "\n";
          }
        }

        if (peer && peer->socklen > 0) {
          sendto(peer->fd.get(), buffer, size, 0, reinterpret_cast<const struct sockaddr*>(&peer->sas), peer->socklen);
        }
*/

        if (debug_) {
          if (x.self_port % 2 == 1) {
            std::cerr << "port: " << x.port << "\n";
            std::cerr << "size: " << size << "\n";
            std::cerr << "self-port: " << x.self_port << "\n";
            std::cerr << "peer-fd: " << x.peer_fd << "\n";
            std::cerr << "peer-host: " << map_[x.peer_fd].host << "\n";
            std::cerr << "peer-port: " << map_[x.peer_fd].port << "\n";
          }
        }

        if (map_[x.peer_fd].socklen > 0) {
          // if (debug_) {
          //   std::cerr << "peer-fd: " << x.peer_fd << "\n";
          //   std::cerr << "peer-host: " << map_[x.peer_fd].host << "\n";
          //   std::cerr << "peer-port: " << map_[x.peer_fd].port << "\n";
          // }
          sendto(
              x.peer_fd, buffer, size, 0,
              reinterpret_cast<const struct sockaddr*>(&map_[x.peer_fd].sas),
              map_[x.peer_fd].socklen);
        }
      }

      if (event & EV_TIMEOUT) {
        item x = map_[fd];
        map_.erase(fd);
        map_.erase(x.pair_fd.get());
        return;
      }

      item x = map_[fd];
      // struct timeval timeout;
      // vaporoid::time::duration(60).get(timeout);
      if (event_add(x.ev.get(), 0) == -1) {
        throw std::runtime_error("could not event_add");
      }
    }
  };
}

#endif
