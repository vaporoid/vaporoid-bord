#include <unistd.h>
#include <iostream>
#include <msgpack.hpp>

int main(int argc, char* argv[]) {
  // 5 read
  // 6 write
  std::map<std::string, std::string> query;
  for (int i = 1; i < argc; i += 2) {
    query[argv[i]] = argv[i + 1];
  }

  msgpack::sbuffer buffer;
  msgpack::pack(&buffer, query);
  write(7, buffer.data(), buffer.size());
  std::cerr << "sent\n";

  msgpack::unpacker unpacker;
  while (true) {
    char c = 0;
    if (read(6, &c, 1) == 1) {
      unpacker.reserve_buffer(1);
      *unpacker.buffer() = c;
      unpacker.buffer_consumed(1);

      msgpack::unpacked result;
      while (unpacker.next(&result)) {
        msgpack::object object = result.get();
        std::cerr << object << "\n";
        return 0;
      }
    }
  }

  return 0;
}
