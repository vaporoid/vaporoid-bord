#ifndef VAPOROID_BORD_CONTEXT_HPP
#define VAPOROID_BORD_CONTEXT_HPP

#include <map>
#include <string>
#include <vector>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

namespace vaporoid_bord {
  class context {
  public:
    explicit context();
    ~context();

    void setup_lua(const std::string& script, const std::vector<std::string>& argument);
    void setup_base();
    void setup_http(const char* host, int port);
    void setup_listener(const char* host, const char* serv);
    void run();
    void shutdown();

    int setup_rtp(const char* host, int port_begin, int port_end);
    void pair_rtp(int port0, int port1);

    void set_debug(bool value);
    void set_nodelay(bool value);
    const std::map<std::string, std::string>& recv_query(int fd) const;
    void send_query(int fd, const std::map<std::string, std::string>& query);
    const std::map<std::string, std::string>& http_query() const;

    std::string uuidgen() const;
    std::string sha1(const std::string& source) const;
    std::string sha512(const std::string& source) const;
    std::string to_jsonp(const std::string& function, const std::map<std::string, std::string>& query) const;
    std::string to_json(const std::map<std::string, std::string>& query) const;

  private:
    class impl;
    boost::scoped_ptr<impl> impl_;
  };

  void set_context(boost::shared_ptr<context> instance);
  context* get_context();
}

#endif
