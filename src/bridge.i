%module vaporoid_bord

%{
#include "context.hpp"
%}

%include std_string.i
%include std_map.i
%include std_vector.i

namespace std {
  %template(map_string_string) map<string, string>;
}

%include context.hpp
