CREATE TABLE IF NOT EXISTS namespace (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  namespace TEXT NOT NULL UNIQUE,
  uri TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE,
  password TEXT,
  password_sha1 TEXT,
  phone TEXT NOT NULL UNIQUE,
  phone_url TEXT NOT NULL DEFAULT 'https://moveablefeast.jp/mfone/ak-phone.html',
  service_url TEXT NOT NULL DEFAULT 'https://moveablefeast.jp/mfone/ak-service.html',
  name TEXT NOT NULL,
  nickname TEXT NOT NULL DEFAULT '',
  note TEXT NOT NULL DEFAULT '',
  is_staff BOOLEAN NOT NULL DEFAULT false,
  namespace INTEGER NOT NULL UNIQUE REFERENCES namespace (id)
);

CREATE TABLE IF NOT EXISTS media_stream (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user INTEGER NOT NULL UNIQUE REFERENCES user (id),
  code TEXT NOT NULL UNIQUE, -- uuid
  fd INTEGER UNIQUE,
  host TEXT NULL,
  port INTEGER NULL,
  created TEXT NOT NULL,
  modified TEXT NOT NULL
);

-- 372ffe
REPLACE INTO user (username, password, password_sha1, phone, phone_url, service_url, name, nickname, note, is_staff)
VALUES (
    'AO13934ZX1', NULL, '18b06d37af7ffb517a0e98118661aa021ad1b805', '03000106011',
    'https://moveablefeast.jp/mfone/phone.html',
    'https://moveablefeast.jp/mfone/service.html',
    '田中太郎', 'お父さん', '優先', 0);

-- 45c34c
REPLACE INTO user (username, password, password_sha1, phone, phone_url, service_url, name, nickname, note, is_staff)
VALUES (
    'AO13934ZX2', NULL, '41acfbe25a46e5cdf5fbcd260ab9e8429146071b', '03000106012',
    'https://moveablefeast.jp/mfone/phone.html',
    'https://moveablefeast.jp/mfone/service.html',
    '田中花子', 'お母さん', '', 0);

-- 5ec4dc
REPLACE INTO user (username, password, password_sha1, phone, phone_url, service_url, name, nickname, note, is_staff)
VALUES (
    'AO13934ZX3', NULL, 'ecb03015dee1cd140f6324a8ea71c118ed6eebe0', '03000106013',
    'https://moveablefeast.jp/mfone/phone.html',
    'https://moveablefeast.jp/mfone/service.html',
    '田中恵子', 'けいこ', '', 0);

REPLACE INTO user (username, password, password_sha1, phone, phone_url, service_url, name, nickname, note, is_staff)
VALUES (
    '03000190001', '03000190001', NULL, '03000190001',
    'https://moveablefeast.jp/mfone/phone.html',
    'https://moveablefeast.jp/mfone/no-service.html',
    'フロント / FRONT', 'フロント / FRONT', '', 1);
