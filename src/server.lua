local ctx = vaporoid_bord.get_context()
local host, path = ...

local SHA1_SALT = "qijt2EgJVo1xnrkVodNIRkxPvCDMoV8BNTJmIn9c"

ctx:set_debug(true)
ctx:set_nodelay(true)

ctx:setup_base()
ctx:setup_http("127.0.0.1", 4280)
ctx:setup_listener(nil, 4217)

function open_db ()
  return sqlite3.open(path)
end

function close_db (db)
  return db:close()
end

function prepare_statement (db, sql)
  return db:prepare(sql)
end

function finalize_statement (sth)
  sth:finalize()
end

function db_select (db, sql, ...)
  local sth = prepare_statement(db, sql)
  for i, v in ipairs({...}) do
    sth:bind(i, v)
  end
  local result = {}
  for i in sth:nrows() do
    table.insert(result, i)
  end
  finalize_statement(sth)
  return result
end

function db_execute (db, sql, ...)
  local sth = prepare_statement(db, sql)
  for i, v in ipairs({...}) do
    sth:bind(i, v)
  end
  sth:step()
  finalize_statement(sth)
end

function make_digest (username, password, code)
  return ctx:sha512(username .. ":" .. password .. ":" .. code)
end

function get_fd_by_phone (db, phone)
  local fd
  local result = db_select(db, [[
    SELECT fd FROM media_stream WHERE user = (SELECT id FROM user WHERE phone = ?);
  ]], phone)
  if #result == 1 then
    fd = result[1].fd
  end
  return fd
end

function cb_media_stream (fd, host, port)
  local query = ctx:recv_query(fd)
  local command = query:get("command")
  local object = vaporoid_bord.map_string_string()

  print("cb_media_stream", fd, host, port, command)

  if command == "REGISTER" then
    local username = query:get("username")
    local digest   = query:get("digest")
    local code     = query:get("code")

    local db = open_db()
    local result = db_select(db, [[
      SELECT u.password AS password, m.id AS id
      FROM user AS u, media_stream AS m
      WHERE u.id = m.user
        AND u.username = ?
        AND m.code = ?
    ]], username, code)

    local ok = false
    if #result == 1 then
      local v = result[1]
      if v.password ~= nil then
        ok = make_digest(username, v.password, code) == digest
      end
    end

    if ok then
      db_execute(db, [[
        UPDATE media_stream SET fd = ?, host = ?, port = ?, modified = datetime('now')
        WHERE id = ?
      ]], fd, host, port, result[1].id)
    end

    close_db(db)

    object:set("command", command)
    object:set("result", ok and "OK" or "NG")
    ctx:send_query(fd, object)
  else
    object:set("result", "NG")
    ctx:send_query(fd, object)
  end
end

-- [TODO] timeout close
function cb_media_stream_close (fd)
  print("cb_media_stream_close", fd)

  local db = open_db()
  db_execute(db, [[
    UPDATE media_stream SET fd = NULL, host = NULL, port = NULL, modified = datetime('now')
    WHERE fd = ?
  ]], fd)
  close_db(db)
end

function cb_http (uri)
  print(uri)

  local query = ctx:http_query()
  local command
  if query:has_key("command") then
    command = query:get("command")
  end
  local callback
  if query:has_key("callback") then
    callback = query:get("callback")
  end

  local object = vaporoid_bord.map_string_string()

  if command == "authenticate" then
    local username = query:get("username")
    local password = query:get("password")
    local password_sha1 = ctx:sha1(SHA1_SALT .. password)

    local db = open_db()
    local result = db_select(db, [[
      SELECT password, password_sha1, phone, phone_url, service_url FROM user
      WHERE username = ?
    ]], username)

    local ok = false
    if #result == 1 then
      local v = result[1]
      if v.password_sha1 ~= nil then
        ok = password_sha1 == v.password_sha1
        if ok then
          db_execute(db, [[
            UPDATE user SET password = ? WHERE username = ?
          ]], password, username)
        end
      end
    end

    local code
    if ok then
      -- [TODO] select and close

      code = ctx:uuidgen()
      db_execute(db, [[
        REPLACE INTO media_stream (user, code, fd, host, port, created, modified)
        VALUES(
          (SELECT id FROM user WHERE username = ?),
          ?,
          NULL,
          NULL,
          NULL,
          datetime('now'),
          datetime('now')
        )
      ]], username, code)
    end
    close_db(db)

    if ok then
      local v = result[1]
      object:set("result", "OK")
      object:set("username", username)
      object:set("password", password)
      object:set("phone", v.phone)
      object:set("phone_url", v.phone_url)
      object:set("service_url", v.service_url)
      object:set("code", code)
    else
      object:set("result", "NG")
    end
  elseif command == "get-users" then
    -- [TODO] authz + authn
    local db = open_db()
    local result = db_select(db, [[
      SELECT
        u.username AS username,
        u.phone AS phone,
        u.name AS name,
        u.nickname AS nickname,
        u.note AS note,
        u.is_staff AS is_staff,
        m.fd AS fd
      FROM user AS u
      LEFT JOIN media_stream AS m ON u.id = m.user
      ORDER BY phone
    ]], username)
    close_db(db)

    local json = "["
    for i, v in ipairs(result) do
      if i > 1 then
        json = json .. ","
      end
      local o = vaporoid_bord.map_string_string()
      o:set("username", v.username)
      o:set("phone", v.phone)
      o:set("name", v.name)
      o:set("nickname", v.nickname)
      o:set("note", v.note)
      print("?", type(v.is_staff), type(v.fd))
      o:set("is_staff", v.is_staff ~= 0 and "is_staff" or "")
      o:set("fd", type(v.fd) == "number" and tostring(v.fd) or "")
      json = json .. ctx:to_json(o)
    end
    json = json .. "]"

    if callback then
      return callback .. "(" .. json .. ")", "application/json; charset=UTF-8"
    else
      return json, "application/json; charset=UTF-8"
    end
  elseif command == "message" then
    local type          = query:get("type")
    local text          = query:get("text")
    local from_phone    = query:get("from_phone")
    local from_name     = query:get("from_name")
    local from_nickname = query:get("from_nickname")
    local to_phone      = query:get("to_phone")
    local to_name       = query:get("to_name")
    local to_nickname   = query:get("to_nickname")

    local db = open_db()
    local fd = get_fd_by_phone(db, to_phone)
    close_db(db)

    print("fd", fd)

    local ok = false
    if fd then
      local o = vaporoid_bord.map_string_string()
      o:set("command",       "MESSAGE")
      o:set("type",          type)
      o:set("text",          text)
      o:set("from_phone",    from_phone)
      o:set("from_name",     from_name)
      o:set("from_nickname", from_nickname)
      o:set("to_phone",      to_phone)
      o:set("to_name",       to_name)
      o:set("to_nickname",   to_nickname)
      ctx:send_query(fd, o)
      ok = true
    end

    object:set("result", ok and "OK" or "NG")
  elseif command == "accept" then
    local from_phone = query:get("from_phone")
    local to_phone   = query:get("to_phone")

    local port0 = ctx:setup_rtp(host, 2000, 2100)
    local port1 = ctx:setup_rtp(host, 3000, 3100)
    ctx:pair_rtp(port0, port1)
    ctx:pair_rtp(port0+1, port1+1)

    local db = open_db()
    do
      local fd   = get_fd_by_phone(db, from_phone)
      local port = port0

      if fd then
        local q = vaporoid_bord.map_string_string()
        q:set("command", "CONNECT")
        q:set("host", host)
        q:set("port", tostring(port))
        ctx:send_query(fd, q)
      end
    end

    do
      local fd   = get_fd_by_phone(db, to_phone)
      local port = port1

      if fd then
        local q = vaporoid_bord.map_string_string()
        q:set("command", "CONNECT")
        q:set("host", host)
        q:set("port", tostring(port))
        ctx:send_query(fd, q)
      end
    end

    close_db(db)

  elseif command == "bye" then
    local from_phone = query:get("from_phone")
    local to_phone   = query:get("to_phone")

    local db = open_db()
    do
      local fd = get_fd_by_phone(db, from_phone)
      if fd then
        local q = vaporoid_bord.map_string_string()
        q:set("command", "DISCONNECT")
        ctx:send_query(fd, q)
      end
    end

    do
      local fd = get_fd_by_phone(db, to_phone)
      if fd then
        local q = vaporoid_bord.map_string_string()
        q:set("command", "DISCONNECT")
        ctx:send_query(fd, q)
      end
    end

    close_db(db)

  elseif command == "cancel" then
    local from_phone = query:get("from_phone")
    local to_phone   = query:get("to_phone")

    local db = open_db()
    do
      local fd = get_fd_by_phone(db, from_phone)

      print(fd)

      local q = vaporoid_bord.map_string_string()
      q:set("command", "CANCEL")
      ctx:send_query(fd, q)
    end

    do
      local fd = get_fd_by_phone(db, to_phone)

      print(fd)

      local q = vaporoid_bord.map_string_string()
      q:set("command", "CANCEL")
      ctx:send_query(fd, q)
    end

    close_db(db)

  else
    object:set("result", "NG")
  end

  if callback then
    return ctx:to_jsonp(callback, object), "application/json; charset=UTF-8"
  else
    return ctx:to_json(object), "application/json; charset=UTF-8"
  end
end
