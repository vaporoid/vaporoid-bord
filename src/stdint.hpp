#ifndef VAPOROID_STDINT_HPP
#define VAPOROID_STDINT_HPP

#include <inttypes.h>
#include <stdint.h>

namespace vaporoid {
  using ::uint8_t;
  using ::uint16_t;
  using ::uint32_t;
  using ::uint64_t;
  using ::uintmax_t;
}

#endif
