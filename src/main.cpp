#include <unistd.h>
#include <exception>
#include <string>
#include <iostream>
#include <vector>
#include <boost/make_shared.hpp>
#include <boost/scope_exit.hpp>
#include <boost/shared_ptr.hpp>
#include "context.hpp"

extern int   optind;
extern char* optarg;

namespace {
  void print_help(std::ostream& out, const char* program) {
    out << "SYNOPSIS\n"
        << "  " << program << " <script> [<argument>...]\n"
        << "  " << program << " -h\n"
        << "\n"
        << "OPTIONS\n"
        << "  -h\n"
        << "    Print a description of the command line options.\n"
        << "\n"
        << "OPERANDS\n"
        << "  <script>\n"
        << "    The name of the script to be invoked.\n"
        << "  <argument>\n"
        << "    A string to pass as an argument for the invoked script.\n"
        << "\n";
  }
}

int main(int argc, char* argv[]) {
  try {
    while (true) {
      int c = getopt(argc, argv, "h");
      if (c == -1) {
        break;
      }
      switch (c) {
        case 'h':
          print_help(std::cout, argv[0]);
          return 0;
        case '?':
          print_help(std::cout, argv[0]);
          return 1;
      }
    }
    if (optind >= argc) {
      print_help(std::cout, argv[0]);
      return 1;
    }

    std::string script = argv[optind];
    std::vector<std::string> argument;
    for (int i = optind + 1; i < argc; ++i) {
      argument.push_back(argv[i]);
    }

    vaporoid_bord::set_context(boost::make_shared<vaporoid_bord::context>());
    BOOST_SCOPE_EXIT(void) {
      vaporoid_bord::set_context(boost::shared_ptr<vaporoid_bord::context>());
    } BOOST_SCOPE_EXIT_END

    vaporoid_bord::get_context()->setup_lua(script, argument);
    vaporoid_bord::get_context()->run();
    return 0;
  } catch (const std::exception& e) {
    std::cerr << "[ERROR] " << e.what() << "\n";
    return 1;
  } catch (...) {
    std::cerr << "[ERROR] unknown exception\n";
    return 1;
  }
}
