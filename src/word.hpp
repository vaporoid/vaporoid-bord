#ifndef VAPOROID_WORD_HPP
#define VAPOROID_WORD_HPP

#include "stdint.hpp"

namespace vaporoid {
  namespace word_detail {
    template <typename T, bool T_is_big_endian>
    struct word;

    template <>
    struct word<uint16_t, true> {
      static uint8_t* word_to_byte(uint16_t source, uint8_t* target) {
        target[0] = source >> 8;
        target[1] = source;
        return target + sizeof(source);
      }

      static uint16_t byte_to_word(const uint8_t* source) {
        return
            static_cast<uint16_t>(source[0]) << 8
          | static_cast<uint16_t>(source[1]);
      }
    };

    template <>
    struct word<uint32_t, true> {
      static uint8_t* word_to_byte(uint32_t source, uint8_t* target) {
        target[0] = source >> 24;
        target[1] = source >> 16;
        target[2] = source >>  8;
        target[3] = source;
        return target + sizeof(source);
      }

      static uint32_t byte_to_word(const uint8_t* source) {
        return
            static_cast<uint32_t>(source[0]) << 24
          | static_cast<uint32_t>(source[1]) << 16
          | static_cast<uint32_t>(source[2]) <<  8
          | static_cast<uint32_t>(source[3]);
      }
    };

    template <>
    struct word<uint64_t, true> {
      static uint8_t* word_to_byte(uint64_t source, uint8_t* target) {
        target[0] = source >> 56;
        target[1] = source >> 48;
        target[2] = source >> 40;
        target[3] = source >> 32;
        target[4] = source >> 24;
        target[5] = source >> 16;
        target[6] = source >>  8;
        target[7] = source;
        return target + sizeof(source);
      }

      static uint64_t byte_to_word(const uint8_t* source) {
        return
            static_cast<uint64_t>(source[0]) << 56
          | static_cast<uint64_t>(source[1]) << 48
          | static_cast<uint64_t>(source[2]) << 40
          | static_cast<uint64_t>(source[3]) << 32
          | static_cast<uint64_t>(source[4]) << 24
          | static_cast<uint64_t>(source[5]) << 16
          | static_cast<uint64_t>(source[6]) <<  8
          | static_cast<uint64_t>(source[7]);
      }
    };

    template <>
    struct word<uint16_t, false> {
      static uint8_t* word_to_byte(uint16_t source, uint8_t* target) {
        target[0] = source;
        target[1] = source >> 8;
        return target + sizeof(source);
      }

      static uint16_t byte_to_word(const uint8_t* source) {
        return
            static_cast<uint16_t>(source[0])
          | static_cast<uint16_t>(source[1]) << 8;
      }
    };

    template <>
    struct word<uint32_t, false> {
      static uint8_t* word_to_byte(uint32_t source, uint8_t* target) {
        target[0] = source;
        target[1] = source >>  8;
        target[2] = source >> 16;
        target[3] = source >> 24;
        return target + sizeof(source);
      }

      static uint32_t byte_to_word(const uint8_t* source) {
        return
            static_cast<uint32_t>(source[0])
          | static_cast<uint32_t>(source[1]) <<  8
          | static_cast<uint32_t>(source[2]) << 16
          | static_cast<uint32_t>(source[3]) << 24;
      }
    };

    template <>
    struct word<uint64_t, false> {
      static uint8_t* word_to_byte(uint64_t source, uint8_t* target) {
        target[0] = source;
        target[1] = source >>  8;
        target[2] = source >> 16;
        target[3] = source >> 24;
        target[4] = source >> 32;
        target[5] = source >> 40;
        target[6] = source >> 48;
        target[7] = source >> 56;
        return target + sizeof(source);
      }

      static uint64_t byte_to_word(const uint8_t* source) {
        return
            static_cast<uint64_t>(source[0])
          | static_cast<uint64_t>(source[1]) <<  8
          | static_cast<uint64_t>(source[2]) << 16
          | static_cast<uint64_t>(source[3]) << 24
          | static_cast<uint64_t>(source[4]) << 32
          | static_cast<uint64_t>(source[5]) << 40
          | static_cast<uint64_t>(source[6]) << 48
          | static_cast<uint64_t>(source[7]) << 56;
      }
    };

    template <typename T>
    inline uint8_t* word_to_byte(T source, uint8_t* target) {
      return word<T, true>::word_to_byte(source, target);
    }

    template <bool T_is_big_endian, typename T>
    inline uint8_t* word_to_byte(T source, uint8_t* target) {
      return word<T, T_is_big_endian>::word_to_byte(source, target);
    }

    template <typename T, bool T_is_big_endian>
    inline uint8_t* word_to_byte(T source, uint8_t* target) {
      return word<T, T_is_big_endian>::word_to_byte(source, target);
    }

    template <typename T>
    inline uint8_t* word_to_byte(T source, uint8_t* target, bool is_big_endian) {
      return is_big_endian
        ? word<T, true> ::word_to_byte(source, target)
        : word<T, false>::word_to_byte(source, target);
    }

    template <typename T>
    inline T byte_to_word(const uint8_t* source) {
      return word<T, true>::byte_to_word(source);
    }

    template <bool T_is_big_endian, typename T>
    inline T byte_to_word(const uint8_t* source) {
      return word<T, T_is_big_endian>::byte_to_word(source);
    }

    template <typename T, bool T_is_big_endian>
    inline T byte_to_word(const uint8_t* source) {
      return word<T, T_is_big_endian>::byte_to_word(source);
    }

    template <typename T>
    inline T byte_to_word(const uint8_t* source, bool is_big_endian) {
      return is_big_endian
        ? word<T, true> ::byte_to_word(source)
        : word<T, false>::byte_to_word(source);
    }
  }

  using word_detail::word_to_byte;
  using word_detail::byte_to_word;
}

#endif
