#ifndef VAPOROID_JSON_HPP
#define VAPOROID_JSON_HPP

#include <iomanip>
#include <ostream>
#include <sstream>
#include <string>
#include <boost/cstdint.hpp>
#include <boost/io/ios_state.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include "dictionary.hpp"

namespace vaporoid {
  namespace detail {
    class json_quoter {
    public:
      explicit json_quoter(std::ostream& out)
        : out_(out) {}

      template <typename U>
      void operator<<(const U& rhs) const {
        out_ << "\"";
        {
          boost::io::ios_all_saver save(out_);
          out_ << std::hex << std::setfill('0');
          boost::for_each(rhs, *this);
        }
        out_ << "\"";
      }

      void operator()(char value) const {
        // RFC 4627 - 2.5. Strings
        switch (value) {
          case '"':  out_ << "\\\""; break;
          case '\\': out_ << "\\\\"; break;
          case '/':  out_ << "\\/";  break;
          case '\b': out_ << "\\b";  break;
          case '\f': out_ << "\\f";  break;
          case '\n': out_ << "\\n";  break;
          case '\r': out_ << "\\r";  break;
          case '\t': out_ << "\\t";  break;
          default:
            if (0x00 <= value && value <= 0x1F) {
              out_ << "\\u" << std::setw(4) << static_cast<boost::uint16_t>(value);
            } else {
              out_ << value;
            }
        }
      }

    private:
      std::ostream& out_;
    };

    class json_converter {
    public:
      explicit json_converter(std::ostream& out)
        : out_(out), is_first_() {}

      void operator<<(const dictionary& rhs) {
        out_ << "{";
        {
          is_first_ = true;
          boost::for_each(rhs, *this);
          is_first_ = false;
        }
        out_ << "}";
      }

      void operator()(const dictionary::value_type& value) {
        if (is_first_) {
          is_first_ = false;
        } else {
          out_ << ",";
        }
        json_quoter(out_) << value.first;
        out_ << ":";
        json_quoter(out_) << value.second;
      }

    private:
      std::ostream& out_;
      bool is_first_;
    };
  }

  template <typename T>
  inline std::string quote_json(const T& value) {
    std::ostringstream out;
    detail::json_quoter(out) << value;
    return out.str();
  }

  inline std::string to_json(const dictionary& value) {
    std::ostringstream out;
    detail::json_converter(out) << value;
    return out.str();
  }

  inline std::string to_jsonp(const std::string& function, const dictionary& value) {
    std::ostringstream out;
    out << function << "(";
    detail::json_converter(out) << value;
    out << ")";
    return out.str();
  }
}

#endif
