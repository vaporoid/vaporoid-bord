#include <signal.h>

#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/event.h>
#include <event2/http.h>
#include <event2/listener.h>

#include <uuid/uuid.h>

#include <iostream>
#include <boost/current_function.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/noncopyable.hpp>
#include <boost/uuid/sha1.hpp>
#include <lua.hpp>

#include <vaporoid/addrinfo.hpp>
#include <vaporoid/lua.hpp>
#include <vaporoid/shared_fd.hpp>
#include <vaporoid/system_error.hpp>
#include <vaporoid/time.hpp>

#include "context.hpp"
#include "context_impl.hpp"
#include "json.hpp"
#include "hash.hpp"

extern "C" {
  int luaopen_vaporoid_bord(lua_State*);
  int luaopen_lsqlite3(lua_State*);
}

namespace vaporoid_bord {
  namespace {
    boost::shared_ptr<context> instance_;
  }

  inline boost::shared_ptr<struct evutil_addrinfo> server_getaddrinfo(
      const char* host,
      const char* serv,
      const vaporoid::evutil_addrinfo_query& query) {
    struct evutil_addrinfo* result = 0;
    int code = evutil_getaddrinfo(host, serv, query.get(), &result);
    if (code != 0) {
      throw vaporoid::evutil_addrinfo_error("could not getaddrinfo", code);
    }
    return boost::shared_ptr<struct evutil_addrinfo>(result, evutil_freeaddrinfo);
  }

  inline vaporoid::shared_fd open_udp(boost::shared_ptr<struct evutil_addrinfo> ai) {
    vaporoid::shared_fd fd(socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol));
    if (!fd) {
      throw std::runtime_error("could not socket");
    }

    if (evutil_make_socket_nonblocking(fd.get()) == -1) {
      throw std::runtime_error("could not evutil_make_socket_nonblocking");
    }

    // if (evutil_make_listen_socket_reuseable(fd.get()) == -1) {
    //   throw std::runtime_error("could not evutil_make_listen_socket_reuseable");
    // }

    if (bind(fd.get(), ai->ai_addr, ai->ai_addrlen) == -1) {
      throw std::runtime_error("could not bind");
    }

    return fd;
  }

  void set_context(boost::shared_ptr<context> instance) {
    instance_ = instance;
  }

  context* get_context() {
    return instance_.get();
  }

  context::context()
    : impl_(new impl()) {}

  context::~context() {}

  void context::setup_lua(const std::string& script, const std::vector<std::string>& argument) {
    impl_->state_.reset(luaL_newstate(), lua_close);
    if (!impl_->state_) {
      throw std::runtime_error("could not luaL_newstate");
    }

    lua_State* L = impl_->state_.get();

    luaL_openlibs(L);
    luaopen_vaporoid_bord(L);
    luaopen_lsqlite3(L);
    lua_setglobal(L, "sqlite3");

    int code = LUA_OK;
    if ((code = luaL_loadfile(L, script.c_str())) != LUA_OK) {
      throw vaporoid::lua_error("could not luaL_loadfile", code);
    }

    vaporoid::lua_caller caller(L);
    BOOST_FOREACH(const std::string& i, argument) {
      caller.push_argument(i);
    }
    if ((code = caller.run()) != LUA_OK) {
      if (!caller.empty()) {
        std::cerr << "[ERROR] " << caller.get_string(0) << "\n";
      }
      throw vaporoid::lua_error("could not lua_pcall", code);
    }
  }

  void context::setup_base() {
    if (impl_->debug_) {
      std::cout << BOOST_CURRENT_FUNCTION << "\n";
    }

    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
      throw vaporoid::system_error("could not signal");
    }

    impl_->base_.reset(event_base_new(), event_base_free);
    if (!impl_->base_) {
      throw std::runtime_error("could not event_base_new");
    }
  }

  void context::setup_http(const char* host, int port) {
    if (impl_->debug_) {
      std::cout << BOOST_CURRENT_FUNCTION << "\n";
    }

    impl_->http_.reset(evhttp_new(impl_->base_.get()), evhttp_free);
    if (!impl_->http_) {
      throw std::runtime_error("could not evhttp_new");
    }

    evhttp_set_gencb(impl_->http_.get(), &context::impl::cb_http, impl_.get());
    if (evhttp_bind_socket(impl_->http_.get(), host, port) == -1) {
      throw std::runtime_error("could not evhttp_bind_socket");
    }
  }

  void context::setup_listener(const char* host, const char* serv) {
    if (impl_->debug_) {
      std::cout << BOOST_CURRENT_FUNCTION << "\n";
    }

    vaporoid::evutil_addrinfo_query query;
    query.tcp().passive().v4mapped().addrconfig();
    boost::shared_ptr<struct evutil_addrinfo> ai(server_getaddrinfo(host, serv, query));

    impl_->listener_.reset(
        evconnlistener_new_bind(impl_->base_.get(), &context::impl::cb_accept, impl_.get(), LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE, -1, ai->ai_addr, ai->ai_addrlen),
        evconnlistener_free);
    if (!impl_->listener_) {
      throw std::runtime_error("could not evconnlistener_new_bind");
    }

    evconnlistener_set_error_cb(impl_->listener_.get(), &context::impl::cb_accept_error);
  }

  int context::setup_rtp(const char* host, int port_begin, int port_end) {
    if (impl_->debug_) {
      std::cout << BOOST_CURRENT_FUNCTION << "\n";
    }

    vaporoid::evutil_addrinfo_query query;
    query.udp().passive().v4mapped().addrconfig();

    vaporoid::shared_fd fd0;
    vaporoid::shared_fd fd1;
    int port = 0;
    for (port = port_begin; port < port_end; port += 2) {
      try {
        std::string serv = boost::lexical_cast<std::string>(port);
        boost::shared_ptr<struct evutil_addrinfo> ai(server_getaddrinfo(host, serv.c_str(), query));
        fd0 = open_udp(ai);
      } catch (const std::exception& e) {
        if (impl_->debug_) {
          std::cerr << "[WARN] " << e.what() << "\n";
        }
      }

      try {
        std::string serv = boost::lexical_cast<std::string>(port + 1);
        boost::shared_ptr<struct evutil_addrinfo> ai(server_getaddrinfo(host, serv.c_str(), query));
        fd1 = open_udp(ai);
      } catch (const std::exception& e) {
        if (impl_->debug_) {
          std::cerr << "[WARN] " << e.what() << "\n";
        }
      }

      if (fd0 && fd1) {
        if (impl_->debug_) {
          std::cout << "port: " << port << "\n";
        }
        break;
      }
      fd0.reset();
      fd1.reset();
    }

    boost::shared_ptr<struct event> ev(
        event_new(impl_->base_.get(), fd0.get(), EV_READ, &context::impl::cb_udp, impl_.get()),
        event_free);
    if (!ev) {
      throw std::runtime_error("could not event_new");
    }

    // struct timeval timeout;
    // vaporoid::time::duration(60).get(timeout);
    if (event_add(ev.get(), 0) == -1) {
      throw std::runtime_error("could not event_add");
    }

    impl_->map_[fd0.get()] = { fd0, fd1, ev,      "", 0 };
    impl_->map_[fd1.get()] = { fd1, fd0, nullptr, "", 0 };

    impl_->map_[fd0.get()].self_port = port;
    impl_->map_[fd0.get()].socklen = 0;
    impl_->map_[fd1.get()].self_port = port + 1;
    impl_->map_[fd1.get()].socklen = 0;

    if (impl_->debug_) {
      std::cerr << "fd0 " << fd0.get() << "\n";
      std::cerr << "fd1 " << fd1.get() << "\n";
    }

    return port;
  }

  void context::pair_rtp(int port0, int port1) {
    int fd0 = -1;
    int fd1 = -1;
    typedef std::pair<int, item> type;
    BOOST_FOREACH(const type& i, impl_->map_) {
      if (i.second.self_port == port0) {
        fd0 = i.first;
      }
      if (i.second.self_port == port1) {
        fd1 = i.first;
      }
    }

    if (impl_->debug_) {
      std::cerr << "pair fd0: " << fd0 << "\n";
      std::cerr << "pair fd1: " << fd1 << "\n";
      std::cerr << "port0: " << port0 << "\n";
      std::cerr << "port1: " << port1 << "\n";
    }

    if (fd0 != -1 && fd1 != -1) {
      impl_->map_[fd0].peer_port = port1;
      impl_->map_[fd0].peer_fd   = fd1;

      impl_->map_[fd1].peer_port = port0;
      impl_->map_[fd1].peer_fd   = fd0;
    }
  }

  void context::run() {
    event_base_dispatch(impl_->base_.get());
  }

  void context::shutdown() {
    struct timeval t;
    memset(&t, 0, sizeof(t));
    t.tv_sec = 1;
    event_base_loopexit(impl_->base_.get(), &t);
  }

  void context::set_debug(bool value) {
    impl_->debug_ = value;
  }

  void context::set_nodelay(bool value) {
    impl_->nodelay_ = value;
  }

  const std::map<std::string, std::string>& context::recv_query(int fd) const {
    return impl_->message_stream_[fd]->query();
  }

  void context::send_query(int fd, const std::map<std::string, std::string>& query) {
    msgpack::sbuffer buffer;
    msgpack::pack(&buffer, query);

    BOOST_FOREACH(struct bufferevent* i, impl_->bev_) {
      if (bufferevent_getfd(i) == fd) {
        struct evbuffer* output = bufferevent_get_output(i);
        evbuffer_add(output, buffer.data(), buffer.size());
      }
    }
  }

  const std::map<std::string, std::string>& context::http_query() const {
    return impl_->http_query_;
  }

  std::string context::uuidgen() const {
    uuid_t uuid;
    uuid_clear(uuid);
    uuid_generate(uuid);
    char buffer[37] = { 0 };
    uuid_unparse(uuid, buffer);
    return buffer;
  }

  std::string context::sha1(const std::string& source) const {
    boost::uuids::detail::sha1 sha1;
    unsigned int digest[5];

    sha1.process_bytes(source.data(), source.size());
    sha1.get_digest(digest);

    std::ostringstream out;
    out << std::hex << std::setfill('0');
    for (int i = 0; i < 5; ++i) {
      uint32_t v = digest[i];
      out << std::setw(8) << v;
    }
    return out.str();
  }

  std::string context::sha512(const std::string& source) const {
    vaporoid::hash hash("sha512");
    hash.update(source.data(), source.size());
    std::vector<uint8_t> md(hash.message_digest_size());
    hash.finalize(&md[0]);

    std::ostringstream out;
    out << std::hex << std::setfill('0');
    for (uint8_t i : md) {
      out << std::setw(2) << static_cast<uint16_t>(i);
    }
    return out.str();
  }

  std::string context::to_jsonp(const std::string& function, const std::map<std::string, std::string>& query) const {
    return vaporoid::to_jsonp(function, query);
  }

  std::string context::to_json(const std::map<std::string, std::string>& query) const {
    return vaporoid::to_json(query);
  }
}
